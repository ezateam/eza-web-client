<?php

/**
 * index.php
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

/**
 * Define application wide constants here
 */
define('DS', DIRECTORY_SEPARATOR);

define('ROOT', dirname(__FILE__));
define('APP_DIR', 'app');
define('CORE_DIR', APP_DIR . DS . 'Core');
define('EZA_DIR', APP_DIR . DS . 'Eza');
define('PUBLIC_DIR', 'public');
define('WWW_ROOT', ROOT . DS . PUBLIC_DIR);
define('VIEW_ROOT', ROOT . DS . EZA_DIR . DS . 'View');

define('APP_MODE', 'development');

require WWW_ROOT . DS .  'index.php';