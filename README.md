##EZA Web client

###Description
Frontend component of the eza project.

###Installation
Clone this repository and install all required dependencies. Prior doing this
you will need to have node.js/npm, bower and composer installed on your system
for the following to work!

```
git clone git@bitbucket.org:ezateam/eza-web-client.git
composer install
npm install
bower install

```

###Major features
By the end of the development, the prototype shall include _at least_ following
list of features (among other obvious ones):

	* Secure communication with backend
	* Authentication
	* Secure Session handling
	* Native MPEG-DASH playback with HTML5 and MSE (Media source extension)
	* Security
	* E-payment intergration