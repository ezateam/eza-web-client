/**
 * popover.js
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

var Popover = function () {
	"use strict";

	var self = this,
		$document = $(document),
		$popoverLink = $('[data-popover]'),

		setup = function () {
			$popoverLink.on('click', togglePopover);
		},

		position = function () {
			// body...
		},

		togglePopover = function (e) {
			e.preventDefault();

		    if($('.popover.open').length > 0) {
				console.log('closePopover');
		        $('.popover').removeClass('open');
		        return;
		    }

			console.log('openPopover');
		    var popover = $($(this).data('popover'));
		    popover.toggleClass('open')
		    e.stopImmediatePropagation();
		};

	return {
		setup: setup
	};
}