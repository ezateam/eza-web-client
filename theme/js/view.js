/**
 * view.js
 * 
 *  Module that manipulates site view.
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

var ViewHandler = function () {
	"use strict";

	var self = this,
		suscribeBtn = "#suscribe-btn",
		suscribeBtnElem, // Not yet present
		suscribeBtnText = document.documentElement.lang === "en" ? "sign up" : "s'inscrire",
		suscribeBtnHtml = "<a class=\"button navbar-button\" id=\"suscribe-btn\" href=\"./register\">" + suscribeBtnText + "</a>",
		searchBarHtml = "<div class=\"search-bar\" id=\"search-bar\"> \
					    	<span class=\"icon\"><i class=\"fa fa-search\"></i></span> \
					    	<input class=\"u-full-width\" id=\"search\" type=\"text\" placeholder=\"Chercher un film ou une serie...\"> \
					    </div>",
		scrollDownElem = $("#scroll-elem"),
		navBarElem = $("#navbar"),
		navBarMenuElem = $("#navbar-menu"),
		hashLinks = $("a[href*=#]:not([href=#])"),
		$document = $(document),
		$window = $(window),
		headerContent = $("#header-content"),
		spinLoadButton = $("#spinjs-button"),
		scrollTop,
		scrollPos,
		docHeight,
		winHeight,

		setup = function () {
			// Bind events
			hashLinks.bind('click', smoothScroll);
			//$window.bind('scroll', dockNavbar);
			$window.bind('scroll', dockNavbarOnScroll);
			$window.bind('scroll', fadeHeaderContent);
			$window.bind('scroll', removePopover);
			//spinLoadButton.bind('click', spinOnButtonClick);
			$("#slide-left").bind('click', openSideNav);
			$("#sidenav-closebtn").bind('click', closeSideNav);

			fadeHeaderContent();
		},

		dockNavbar = function () {
			winHeight = $window.height();
			scrollTop = $document.scrollTop();

			var offset = 0;
			var isLandingPage = $('.landing').length;

			isLandingPage ? offset = (winHeight - scrollTop - 31) : offset = 61 - scrollTop;

			//console.log("winHeight: " + winHeight);
			//console.log("scrollTop: " + scrollTop);
			//console.log("Offset: " + offset);
			//console.log("winHeight - offset: " + (winHeight - offset));

			if (offset <= 0 ) {
			    if (!navBarMenuElem.find(suscribeBtnElem).length &&
			    	isLandingPage) {
			    	navBarMenuElem.append(suscribeBtnHtml);
			    	// TODO append search bar
			    }
			        
		        navBarElem.removeClass('full');
		        navBarElem.addClass('dock');
			}
		},

		dockNavbarOnScroll = function () {
			docHeight = $document.height();
			//winHeight = $window.height();
			scrollTop = $document.scrollTop();
			suscribeBtnElem = $(suscribeBtn);

			var start = 0;
			var isLandingPage = $('.landing').length;
			var isLoginPage = $('.login-page').length;
			var isRegisterPage = $('.register-page').length;
			var className;
			var dockClassName = 'dock navbar-shadow';

			if (window.trailerOn) {
				navBarElem.addClass('white-bg');
				return;
			}

			//console.log($('.background-overlay').height());

			//console.log("isLandingPage: " + isLandingPage);

			//isLandingPage ? start = winHeight - 60 : start = 1;

			if (isLandingPage || isLoginPage || isRegisterPage) {
				//start = winHeight - 60;
				start = $('.background-overlay').height() - 60;
				className = 'full';
			} else {
				start = 1;
				className = 'full navbar-shadow';
			}

			//console.log("className: " + className);
			//console.log("scrollTop: " + scrollTop);

			// 60 is the header height - FIXME: dynamically extract header height
			if (scrollTop >= start) {
			    // Append suscribe button
			    if (!navBarMenuElem.find(suscribeBtn).length &&
			    	isLandingPage) {
			        navBarMenuElem.append(suscribeBtnHtml);
			    }
			    
			    // TODO append search too
			    
			    navBarElem.removeClass(className);
			    navBarElem.addClass(dockClassName);
			    
			} else {
			    navBarElem.removeClass(dockClassName);
			    navBarElem.addClass(className);

			    // Remove suscribe button
			    if (navBarMenuElem.find(suscribeBtn).length)
			    	suscribeBtnElem.remove();
			    // TODO remove search bar
			}
		},

		fadeHeaderContent = function () {
			scrollPos = $document.scrollTop();
			    
		    headerContent.css({
		        'opacity': 1 - (scrollPos / 450)
		    });
		},

		smoothScroll = function (e) {
			docHeight = $document.height();
			winHeight = $window.height();

			var docOffset = docHeight - winHeight;
			var dest = 0;

			e.preventDefault();

			// check if destinationis within the viewport window
			if ($(this.hash).offset().top > docOffset) {
			    dest = docOffset;
			} else {
			    dest = $(this.hash).offset().top;
			    //take off the header height from result - FIXME get heder height dynamically
			    dest = dest - 60;
			}
			
			//alert(dest);
			$('html,body').animate({scrollTop:dest}, 1300, 'swing');

		},

		toggleSideNav = function (e) {
			var isOpen = Boolean(document.getElementById("sidenav-left").style.width);

			e.preventDefault();

			if (isOpen) {
				document.getElementById("sidenav-left").style.width = "";
				//document.getElementById("main").style.marginLeft = "";
			} else {
				document.getElementById("sidenav-left").style.width = "250px";
				//document.getElementById("main").style.marginLeft = "250px";
			}
		},

		openSideNav = function (e) {
			e.preventDefault();
			document.getElementById("sidenav-left").style.width = "250px";
		},

		closeSideNav = function (e) {
			e.preventDefault();
			document.getElementById("sidenav-left").style.width = "0";
		},

		spinOnButtonClick = function (e) {
			e.preventDefault();
			console.log('spinLoadButton');
			var opts = {
			  lines: 11,
			  length: 3,
			  width: 2,
			  radius:4,
			  left: '20px'
			};

			var spinner = new Spinner(opts);


			spinner.spin(spinLoadButton);
		},

		removePopover = function () {
		    if($('.popover').length > 0) {
		        $('.popover').hide();
		    }
		};

	return {
		setup: setup,
		dockNavbar: dockNavbar
	};

}