/**
 * flash-notify.js
 * 
 * @author Zangue <armand.zangue@gmail.com>
 */

var FlashNotify = function () {
	"use-strict";

	var self = this,
		flashElem = "#flash-message",

		notify = function () {
			var flash = $(flashElem),
				type,
				//title,
				text,
				prefix;

			if (!flash.length)
				return;	

            type = flash.find("#flash-type")[0].innerHTML;
            
			if (type === "info")
				//title = "Info";
				prefix = '<span><i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i></span>&nbsp;';
			else if (type === "error")
				//title = "Erreur";
				prefix = '<span><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i></span>&nbsp;';
			else if (type === "success")
				//title = "Terminé";
				prefix = '<span><i class=\"fa a-check-circle-o\" aria-hidden=\"true\"></i></span>&nbsp;';

			//console.log(type);

			text = prefix + flash.find("#flash-text")[0].innerHTML;

			new PNotify ({
				text: text,
				type: type,
				width: '450px',
				min_height: "25px",
				icon: false,
				buttons: {
				    closer: true,
				    closer_hover: false,
				    sticker: false,
				    classes: {
			            closer: 'fa fa-close',
			        }
				}
			});

		};

	return {
		notify: notify
	};
}