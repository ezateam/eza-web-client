/**
 * main.js
 * 
 * @author Zangue <armand.zangue@gmail.com>
 */

$(document).ready(function () {
    var viewHandler = new ViewHandler();
    var trailer = new Trailer();
    var flash = new FlashNotify();
    var popover = new Popover();

    window.trailerOn = false;

    flash.notify();
    popover.setup();
    viewHandler.setup();
    viewHandler.dockNavbar();

    trailer.setup();

    // Social Buttons
    $("#social-buttons").jsSocials({
    	//url: ,
    	//text: ,
    	showLabel: false,
    	showCount: false,
        shares: ["facebook", "twitter", {share: "email", logo: "fa fa-envelope"}]
    });
});
