/**
 * trailer.js
 * 
 * @author Zangue <armand.zangue@gmail.com>
 */

var Trailer = function () {
	"use strict";

	var self = this,
		trailerBtn = $("#trailer-button"),
		closeTrailerBtn = $("#trailer-overlay-close"),
		videoEl,
		player,

		setup = function () {
			// TODO - Get the mpd name and url
			trailerBtn.bind('click', displayTrailer);
			closeTrailerBtn.bind('click', closeTrailer);
			//window.onresize = onResize; // TODO
		},

		onResize = function () {
			console.log('onResize');
			var fakeEvent = {
				preventDefault: function () {return;}
			};

			if (window.trailerOn) {
				closeTrailer.call(self, fakeEvent);
				displayTrailer.call(self, fakeEvent);
			}
		},

		setupPlayer = function () {
			self.player.ready(function() {
				//self.player.on('ended', self.closeTrailer);
				self.player.src({
					src: 'https://bitdash-a.akamaihd.net/content/MI201109210084_1/mpds/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.mpd',
					type: 'application/dash+xml'
				});
			});
		},

		displayTrailer = function (e) {

			e.preventDefault();

			var overlayEl = document.getElementById("trailer-overlay");
			var marginTop = document.getElementById("navbar").offsetHeight;
			var videoHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0) - Number(marginTop);

			overlayEl.style.marginTop = String(marginTop) + 'px';
			overlayEl.style.height = "100%";

			// Remover around 6% of trailer header
			videoHeight -= Math.round(videoHeight * 0.08);

			window.trailerOn = true;

			if (self.player) {
				setupPlayer();
				return;
			}

			var trailerDiv = document.getElementById("trailer-div");
			var videoEl = document.createElement('video');

			videoEl.id = 'video-player';
			videoEl.setAttribute('controls', '');
			videoEl.style.width = '700px'; //'100%';
			videoEl.style.height = '480px' //String(videoHeight) + 'px';
			videoEl.className = 'video-js vjs-default-skin';

			trailerDiv.appendChild(videoEl);

			self.videoEl = videoEl;

			var player = videojs('video-player');
			self.player = player;

			setupPlayer();
		},

		closeTrailer = function (e) {
			console.log('closeTrailer');
			e.preventDefault();

			if (self.player)
				self.player.reset();

			document.getElementById("trailer-overlay").style.height = "0";

			window.trailerOn = false;
		};

	return {
		setup: setup,
	};
}