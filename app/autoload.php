<?php

/**
 * app/autoload.php
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

require_once CORE_DIR . DS . 'Autoloader.php';

$coreAutoloader = new \Core\Autoloader();

$coreAutoloader->register();