<?php
/**
 * app/bootstrap.php
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

// Require application autoloader
require APP_DIR . DS . 'autoload.php';
// Require all PHP dependencies using the composer autoloader
require ROOT . DS . 'vendor' . DS . 'autoload.php';

require EZA_DIR . DS . 'CustomerClient.php';
require EZA_DIR . DS . 'PartnerClient.php';

$requestUriParts = explode('/', trim($_SERVER['REQUEST_URI']));
$isPartnerRequest = in_array('partner', $requestUriParts);

//$start = microtime(true);

if ($isPartnerRequest) {
    $client = new \Eza\PartnerClient();
} else {
    $client = new \Eza\CustomerClient();
}

$client->run();

//$time_elapsed_secs = microtime(true) - $start;

//var_dump($time_elapsed_secs); die('b');