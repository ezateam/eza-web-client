<?php

/**
 * app/Core/View/View.php
 *
 * @author Zangue
 */

namespace Core\View;

class View {

    /**
     * Slim application instance
     * @var Slim
     */
    protected $app;

    /**
     * Layout file name
     * @var string
     */
    protected $layout;

    /**
     * Data for the view
     * @var Array
     */
    private $viewData = array();


    public function __construct($app, $viewData) {

        $this->app = $app;
        $this->viewData = array_merge($this->viewData, $viewData);
        $this->layout = $this->app->config->get(
            'app.settings.renderer.layout_filename');
    }


    public function setLayout ($fname) {
        $this->layout = $fname;
    }

    /**
     * Render view for specific action
     * @param  string $dir  directory name that contains the view files
     * @param  string $view view file
     * @return void
     */
    public function render($dir, $view) {

        $templatePath = $this->app->config->get(
            'app.settings.renderer.template_path');
        $templateExt = $this->app->config->get(
            'app.settings.renderer.template_ext');

        $file = $templatePath . DS . $dir . DS . $view . $templateExt;

        $this->viewData['app'] = $this->app;
        $this->viewData['csrf_key'] = $this->app->csrfInfos['csrf_key'];
        $this->viewData['csrf_token'] = $this->app->csrfInfos['csrf_token'];
        
        ob_start();

        extract($this->viewData);
        
        require $file;

        $content = ob_get_clean();

        // We don't need to built complete view for ajax request
        if ($this->app->request->isAjax()) {
            $this->app->response->setBody($content);
            return; 
        }

        // Store slim app to pass it to the view
        $app = $this->app;

        // Append data to view
        $this->app->view->appendData(compact('app', 'content'));

        $this->app->render($this->layout);

    }

}