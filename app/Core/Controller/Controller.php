<?php

/**
 * app/Core/Controller/Controller.php
 *
 * @author Zangue
 */

namespace Core\Controller;

use Core\View\View;

class Controller {

    /**
     * Slim application instance
     * @var Slim
     */
    protected $app;

    /**
     * Current model class
     * @var Model
     */
    protected $model;

    /**
     * The name of the view to use.
     * @var String
     */
    protected $viewName = NULL;

    /**
     * The name of the layout file to use.
     * @var String
     */
    protected $layout = "";

    /**
     * Data for the view
     * @var array
     */
    private $viewData = array();


    public function __construct($app, $modelClass = NULL) {

        $this->app = $app;
        $this->model = $modelClass;
    }

    protected function setLayout($fname) {
        $this->layout = $fname;
    }


    /**
     * Set data to be passed to the view
     * @param array $data data
     */
    protected function set($data = null) {

       if (isset($data))
            $this->viewData = array_merge($this->viewData, $data);
    }

    /**
     * Set view to use. This can be useful if one wants to render custom views
     * @param string $name
     */
    protected function setView ($name) {
        $this->viewName = $name;
    }

    /**
     * Render View
     * @param  string $dir View directory
     * @param  sring $viewName View file name (without extension)
     */
    public function render($dir, $viewName) {

        $view = new View($this->app, $this->viewData);

        if (!empty($this->layout))
            $view->setLayout($this->layout);

        $viewName = isset($this->viewName) ? $this->viewName : $viewName;

        $view->render($dir, $viewName);
    }
}