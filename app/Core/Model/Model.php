<?php

/**
 * app/Core/Model/Model.php
 *
 * @author Zangue
 */

namespace Core\Model;

class Model {

    /**
     * Slim application instance
     * @var Slim
     */
    protected $app;

    /**
     * Class constructor
     * @param Slim $app
     */
    public function __construct($app) {

        $this->app = $app;
    }

}