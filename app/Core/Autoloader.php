<?php

/**
 * app/Core/Autoloader.php
 *
 * @author Zangue
 */

namespace Core;

class Autoloader {

    /**
     * PSR-0 Autoloader
     *
     * @see http://www.php-fig.org/psr/psr-0/
     * @param  string $className The name of the class to load
     * @return void
     */
    public function autoload($className) {

        $className = ltrim($className, '\\');
        $fileName  = '';
        $namespace = '';

        if ($lastNsPos = strrpos($className, '\\')) {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName  = APP_DIR . DS .
                        str_replace('\\', DIRECTORY_SEPARATOR, $namespace) .
                        DIRECTORY_SEPARATOR;
        }

        $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

        //echo "Try to load file " . $fileName . "<br>";

        if (file_exists($fileName)) {
            require $fileName;
        }
    }

    /**
     * Register autoloader
     * @return None
     */
    public function register() {
        spl_autoload_register(array($this, 'autoload'));
    }

}