<?php

/**
 * app/Core/Router.php
 *
 * @author Zangue
 */

namespace Core;

class Router {

    /**
     * $app Slim Application instance
     * @var Slim
     */
    private $app;


    public function __construct($app) {
        $this->app = $app;
    }


    /**
     * Generic routes handler
     * @param  String $method Http method, e.g. GET, POST
     * @param  String $url    method url
     * @param  String $action action to perform
     * @return Closure
     */
    private function _call($method, $url, $action, $acl) {

        if ($acl === NULL) {
            $acl = function() {};
        }

        return $this->app->map($url, $acl, function () use ($action) {
            
            $action = explode('@', $action);

            $modelClass = rtrim($action[0], 's') . 'Model';
            $modelClassFile = ROOT . DS . EZA_DIR . DS . 'Model' . DS .
                                $modelClass . '.php';
            $model = NULL;

            if (file_exists($modelClassFile)) {
                $modelClass = "Eza\\Model\\" . $modelClass;
                $model = new $modelClass($this->app);
            }

            $controllerClass = $action[0] . 'Controller';
            // TODO: check if file exists. Throw exception if it doesn't
            $controllerClassFile = ROOT . DS . EZA_DIR . DS . 'Controller' .
                                        DS . $controllerClass . '.php';
            
            $controllerClass = "Eza\\Controller\\" .$action[0] . 'Controller';
            $method = $action[1];

            $controller = new $controllerClass($this->app, $model);

            // Add Backend -----------------------------------------------------
            $backendClass = "Eza\\Backend\\Backend";
            $backend = new $backendClass($this->app);
            $controller->setBackend($backend);
            if ($model) $model->setBackend($backend);
            // -----------------------------------------------------------------

            // Add error handler -----------------------------------------------
            $errorHandlerClass = $action[0] . 'ErrorHandler';
            $errorHandlerClassFile =  ROOT . DS . EZA_DIR . DS . 'ErrorHandler'
                                        . DS . $errorHandlerClass . '.php';

            if (file_exists($errorHandlerClassFile)) {
                $errorHandlerClass = "Eza\\ErrorHandler\\" . $errorHandlerClass;
                $controller->setErrorHandler(
                    new $errorHandlerClass($this->app, $controller)
                );
            }
            // -----------------------------------------------------------------

            // Execute controller ----------------------------------------------
            call_user_func_array([$controller, $method], func_get_args());
            // -----------------------------------------------------------------

            // Render view if no redirection has been set
            if ($this->app->response->getStatus() !== 302) {
                $viewDir = $this->app->language . DS . $action[0];
                $viewName = $method;
                call_user_func_array(
                    [$controller, 'render'],
                    [$viewDir, $viewName]
                );
            }
        })->via($method);
    }

    /*
     * TODO
     */
    protected function getLangPrefix () {
        return $this->app->langURLPrefix;
    }

    /**
     * Routes http GET request
     * @param  String $url    GET url
     * @param  Closure $action action to perform
     * @param  Closure $acl
     * @return Closure
     */
    public function get($url, $action, $acl = NULL) {
        
        return $this->_call(
            'GET',
            $this->getLangPrefix() . $url,
            $action,
            $acl
        );
    }


    /**
     * Route http POST request
     * @param  String $url    POST url
     * @param  Closure $action action to perform
     * @param  Closure $acl
     * @return Closure
     */
    public function post($url, $action, $acl = NULL) {
        
        return $this->_call(
            'POST',
            $this->getLangPrefix() . $url,
            $action,
            $acl
        );
    }

    /**
     * Handle errors
     * @param  string $code error type, eg 404, 500
     * @return Closure
     */
    public function error($code) {

        $model = NULL;
        $modelClass = 'Eza\\Model\\ErrorModel';
        $modelClassFile = ROOT . DS . EZA_DIR . DS . 'Model' . DS .
                            'ErrorModel.php';

        if (file_exists($modelClassFile))
                $model = new $modelClass($this->app);

        $controllerClass = 'Eza\\Controller\\ErrorsController';
        $controller = new $controllerClass($this->app, $model);

        $method = 'error' . $code;

        return call_user_func([$controller, $method]);
    }

}