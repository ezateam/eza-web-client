<?php

/**
 * conifg/production.php
 * 
 * Configuration file while in production mode.
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

return [
	'app' => [
		'url' => 'http://52.28.205.249',
		'partner_url' => 'http://52.28.205.249/partner',

		'version' => '1.0.0',
		
		'settings' => [
			'displayErrorDetails' => false,

			'renderer' => [
				'template_path' => VIEW_ROOT,
				'template_ext' => '.html',
				'layout_filename' => 'layout.html'
			]
		]
	],

	'api' => [
		'backend' => [
			//'base_url' => 'https://52.28.205.249:8443/eza-1',
			'base_url' => 'https://52.28.205.249:8443',
			//'base_url' => 'http://46.101.246.80:8080/eza-1',
			//'base_url' => 'http://46.101.246.80:8080',
			'accesskey' => 'ODExYWQ5YTAtZWFhYS00NmQ1LWI4MGQtNjE1ODU1ZmEwNTI2OmNlNjE5ODI0LThjMTEtNDA3ZC1iZjJkLWUxNGU3ZmM4NmQwZg==',
			'tokenkey' => 'Y2U2MTk4MjQtOGMxMS00MDdkLWJmMmQtZTE0ZTdmYzg2ZDBmOjgxMWFkOWEwLWVhYWEtNDZkNS1iODBkLTYxNTg1NWZhMDUyNg=='
		],

		'phpsession-db' => [
			'base_url' => 'http://',
			'accesskey' => ''
		]
	],

	'auth' => [
		'session' => 'user',
		'remember' => 'user_r',
		'login_redirect' => 'catalog'
	],

	'csrf' => [
		'key' => 'csrf_token'
	],

	'i18n' => [
		'default' => 'fr',
		'available' => ['fr', 'en']
	]
];