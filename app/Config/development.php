<?php

/**
 * conifg/development.php
 * 
 * Configuration file while in development mode.
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

return [
	'app' => [
		'url' => 'http://localhost/eza-web-client',
		'partner_url' => 'http://localhost/eza-web-client/partner',

		'version' => '1.0.0',
		
		'settings' => [
			'displayErrorDetails' => true,

			'renderer' => [
				'template_path' => VIEW_ROOT,
				'template_ext' => '.html',
				'layout_filename' => 'layout.html'
			]
		]
	],

	'api' => [
		'backend' => [
			//'base_url' => 'https://52.28.205.249:8443/eza-1',
			'base_url' => 'https://52.28.205.249:8443',
			//'base_url' => 'http://46.101.246.80:8080/eza-1',
			//'base_url' => 'http://46.101.246.80:8080/eza',
			//'base_url' => 'http://46.101.246.80:8080',
			'accesskey' => 'ODExYWQ5YTAtZWFhYS00NmQ1LWI4MGQtNjE1ODU1ZmEwNTI2OmNlNjE5ODI0LThjMTEtNDA3ZC1iZjJkLWUxNGU3ZmM4NmQwZg==',
			'tokenkey' => 'Y2U2MTk4MjQtOGMxMS00MDdkLWJmMmQtZTE0ZTdmYzg2ZDBmOjgxMWFkOWEwLWVhYWEtNDZkNS1iODBkLTYxNTg1NWZhMDUyNg=='
		],

		'phpsession-db' => [
			'base_url' => 'http://localhost/phpsession-db/api/v1',
			'accesskey' => ''
		]
	],

	'auth' => [
		'session' => 'user',
		'remember' => 'user_r',
		'login_redirect' => 'catalog',
		'partner_login_redirect' => 'partner-dashboard'
	],

	'csrf' => [
		'key' => 'csrf_token'
	],

	'i18n' => [
		'default' => 'fr',
		'available' => ['fr', 'en']
	]
];