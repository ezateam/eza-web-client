<?php
/**
 * Backend.php
 * 
 * @desc Takes care of all operations with the backend
 * @author Zangue
 */

namespace Eza\Backend;

use Eza\Interfaces\Backend\BackendInterface;

use Pest;
use PestJSON;

class Backend implements BackendInterface {

    /**
     * Slim Application
     * @var Slim
     */
    protected $app;

    /**
     * Eza Backend api key
     * @var String
     */
    protected $apikey;

    /**
     * Rest object factory
     * @var Eza\Lib\RESTFactory;
     */
    protected $restFactory;

    /**
     * Authorization value field
     * @var string
     */
    protected $authorization;

    /**
     * Contructor
     * @param Slim $app
     */
    public function __construct ($app) {
        $this->app = $app;
        $this->apikey = $this->app->config->get('api.backend.accesskey');
        $this->restFactory = $this->app->restFactory;
        $this->authorization = 'Bearer ' . $this->app->access_token;
    }

    /*
     * User controller ---------------------------------------------------------
     */

    /**
     * Log user in
     * @param  string $username
     * @param  string $password
     * @return Eza\Lib\REST
     */
    public function login ($username, $password) {
        $endpoint = '/login';

        $rest = $this->restFactory->create()
            ->url($endpoint)
            ->withHeader('eza-apikey', $this->apikey)
            ->contentType('application/x-www-form-urlencoded')
            ->data('username', $username)
            ->data('password', $password)
            ->buildHttpQuery()
            ->viaPost()
            ->execute();

        return $rest;
    }

    /**
     * Register new user
     * @param  string $email
     * @param  string $username
     * @param  string $password
     * @return Eza\Lib\REST
     */
    public function register ($email, $username, $password, $role) {
        $endpoint = '/registration/newuser';

        $rest = $this->restFactory->createJSON()
            ->url($endpoint)
            ->contentType('application/json')
            ->withHeader('Authorization', $this->authorization)
            ->data('role', $role)
            ->data('email', $email)
            ->data('username', $username)
            ->data('password', $password)
            ->viaPost()
            ->execute();

        return $rest;
    }

    /**
     * Confirm user registration with supplied token
     * @param  string $token
     * @return Eza\Lib\REST
     */
    public function confirmRegistration ($token) {
        $endpoint = '/registration/confirmRegistration';

        $rest = $this->restFactory->create()
            ->url($endpoint)
            ->contentType('application/x-www-form-urlencoded')
            ->withHeader('Authorization', $this->authorization)
            ->data('verificationToken', $token)
            ->viaGet()
            ->execute();

        return $rest;
    }

    /**
     * Send a new verification token to user
     * @param  string $username
     * @return Eza\Lib\REST
     */
    public function resendVerificationToken ($username) {
        $endpoint = '/registration/sendNewVerificationToken';

        $rest = $this->restFactory->create()
            ->url($endpoint)
            ->contentType('application/x-www-form-urlencoded')
            ->withHeader('Authorization', $this->authorization)
            ->data('identify', $username)
            ->viaPost()
            ->execute();

        return $rest;
    }

    /**
     * Send link for password reset
     * @param  string $username
     * @return Eza\Lib\REST
     */
    public function forgotPassword ($username) {
        $endpoint = '/users/password/forget';

        $rest = $this->restFactory->create()
            ->url($endpoint)
            ->contentType('application/x-www-form-urlencoded')
            ->withHeader('Authorization', $this->authorization)
            ->data('identify', $username)
            ->viaPost()
            ->execute();

        return $rest;
    }

    /**
     * Run checks on verification token
     * @param  string $token
     * @return Eza\Lib\REST
     */
    public function handleVerificationToken ($token) {
        $endpoint = '/users/password/handleverificationtoken?verificationToken=' . $token;

        $rest = $this->restFactory->create()
            ->url($endpoint)
            ->withHeader('Authorization', $this->authorization)
            ->viaGet()
            ->execute();

        return $rest;
    }

    /**
     * Reset user password
     * @param string $token
     * @param string $password  New password
     * @return Eza\Lib\REST
     */
    public function resetPassword ($token, $password) {
        $endpoint = '/users/password/reset';

        $rest = $this->restFactory->create()
            ->url($endpoint)
            ->withHeader('Authorization', $this->authorization)
            ->contentType('application/x-www-form-urlencoded')
            ->data('verificationToken', $token)
            ->data('password', $password)
            ->viaPost()
            ->execute();

        return $rest;
    }

    /*
     * User model --------------------------------------------------------------
     */
    
    /**
     * GEt user infos
     * @param  String $identifier User name or email
     * @return Eza\Lib\REST
     */
    public function getUser ($identifier) {
        $endpoint = '/users/byemailorname/' . trim($identifier) . '/';

        $rest = $this->restFactory->create()
            ->url($endpoint)
            ->withHeader('Authorization', $this->authorization)
            ->viaGet()
            ->execute();

        return $rest;
    }

    /**
     * Get user session infos
     * @param  String $userUUId
     * @param  String $sessionId
     * @return Eza\Lib\REST
     */
    public function getUserSession ($userUUId, $sessionId) {
        $endpoint =  '/usersession/byuseruuidandsessionid/' . trim($userUUId) . '/' . trim($sessionId);

        $rest = $this->restFactory->create()
            ->url($endpoint)
            ->withHeader('Authorization', $this->authorization)
            ->viaGet()
            ->execute();

        return $rest;
    }

    /**
     * Get all session infos attached to specific user
     * @param  String $userUUId
     * @return Eza\Lib\REST
     */
    public function getAllUserSessions ($userUUId) {
        $endpoint = '/usersession/byuseruuid/' . trim($userUUId);

        $rest = $this->restFactory->create()
            ->url($endpoint)
            ->withHeader('Authorization', $this->authorization)
            ->viaGet()
            ->execute();

        return $rest;
    }

    /**
     * Store user session infos
     * @param  Array $data  Session Infos
     * @return Eza\Lib\REST
     */
    public function storeUserSession ($data) {
        $endpoint = '/usersession/add';

        $rest = $this->restFactory->create()
            ->url($endpoint)
            ->contentType('application/json')
            ->withHeader('Authorization', $this->authorization)
            ->arrayData($data)
            ->viaPost()
            ->execute();

        return $rest;
    }

    /**
     * Delete session infos
     * @param  String $sessionId
     * @return Eza\Lib\REST
     */
    public function deleteUserSession ($sessionId) {
        $endpoint = '/usersession/delete/' . trim($sessionId);

        $rest = $this->restFactory->create()
            ->url($endpoint)
            ->withHeader('Authorization', $this->authorization)
            ->viaDelete()
            ->execute();

        return $rest;
    }

}