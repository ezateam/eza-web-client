<?php
/**
 * PartnersErrorHandler.php
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

namespace Eza\ErrorHandler;

use Eza\Lib\Util;

class PartnersErrorHandler extends UsersErrorHandler {

    protected $isPartner = true;

}