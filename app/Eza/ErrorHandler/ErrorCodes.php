<?php
/**
 * ErrorCodes.php
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

namespace Eza\ErrorHandler;

class ErrorCodes {

    /**
     * EZA error codes
     */
    const UUID_ERROR = 'EZA_001';
    const LOGIN_ERROR = 'EZA_002';
    const USER_ALREADY_ENABLED_ERROR = 'EZA_003';
    const TIME_EXPIRED_ERROR = 'EZA_004';
    const USER_NOT_ENABLE_ERROR = 'EZA_005';
    const NO_ENTITY_FOUND = 'EZA_006';


    /**
     * Slim application
     * @var Slim
     */
    protected $app;

    protected $controller;

    public function __construct ($app, $controller) {
        $this->app = $app;
        $this->controller = $controller;
    }
}