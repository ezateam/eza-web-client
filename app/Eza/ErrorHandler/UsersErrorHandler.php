<?php
/**
 * UsersErrorHandler.php
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

namespace Eza\ErrorHandler;

use Eza\Lib\Util;
use Eza\Component\Message;

class UsersErrorHandler extends ErrorCodes {

    protected $isPartner = false;

    public function handleLoginError ($errorCode, $exception) {

        if (!empty($this->app->request->post()))
            $this->app->session->set('post-data', array(
                    'username' => $this->app->request->post('username')
                )
            );

        switch ($errorCode) {
            case self::LOGIN_ERROR:
                $this->app->flash(
                    'error',
                    $this->app->messages->get(
                        Message::USER_LOGIN_WRONG_CREDENTIALS,
                        $this->app->language
                    )
                );
                if ($this->isPartner)
                    $this->app->redirect($this->app->urlFor('partner-login'));
                else
                    $this->app->redirect($this->app->urlFor('login'));
                break;

            case self::USER_NOT_ENABLE_ERROR:
                $hash = Util::hash($this->app->randomlib->generateString(128));
                $this->app->session->set('hash', $hash);
                $this->app->session->set('account-not-activated', true);
                $this->app->redirect(
                    $this->app->urlFor('register') .
                    "?status=account-not-activated&username=" .
                    $this->app->request->post('username') .
                    "&hash=". $hash
                );
                break;

            case self::NO_ENTITY_FOUND:
                $this->app->flash(
                    'error',
                    $this->app->messages->get(
                        Message::USER_NOT_FOUND,
                        $this->app->language
                    )
                );
                if ($this->isPartner)
                    $this->app->redirect($this->app->urlFor('partner-login'));
                else
                    $this->app->redirect($this->app->urlFor('login'));
                break;

            default:
                $this->app->flash(
                    'error',
                    $this->app->messages->get(
                        Message::USER_POSTED_DATA_PROCESSING_FAILED,
                        $this->app->language
                    ) . " (110)" . $exception->getMessage()
                );
                if ($this->isPartner)
                    $this->app->redirect($this->app->urlFor('partner-login'));
                else
                    $this->app->redirect($this->app->urlFor('login'));
                break;
       }
    }

    public function handlerRegisterError ($errorCode, $exception) {
        if (!empty($this->app->request->post()))
            $this->app->session->set('post-data', array(
                    'username' => $this->app->request->post('username'),
                    'email' => $this->app->request->post('email')
                )
            );

        // TODO - server should give error code back
        //var_dump($exception->getMessage()); die();
        //$this->app->flashNow('error', $exception->getMessage());
        $this->app->flashNow(
            'error',
            $this->app->messages->get(
                Message::USER_REGISTER_ID_TAKEN,
                $this->app->language
            )
            //$exception->getMessage()
        );
        header("Location: " . $this->app->urlFor('register'));
    }

    public function handleConfirmRegistrationError ($errorCode, $exception) {
        $username = $this->app->request->get('identify');

        switch ($errorCode) {
            case self::USER_ALREADY_ENABLED_ERROR:
                $this->app->flash(
                    'info',
                    $this->app->messages->get(
                        Message::USER_ACCOUNT_ALREADY_ACTIVATED,
                        $this->app->language
                    )
                );
                $this->app->redirect($this->app->urlFor('login'));
                break;

            // Token has expired
            case self::TIME_EXPIRED_ERROR:
                $hash = Util::hash($this->app->randomlib->generateString(128));

                $this->app->session->set('token-expired', true);
                $this->app->session->set('hash', $hash);

                $urlData = array(
                    'status' => 'token-expired',
                    'username' => $username,
                    'hash' => $hash
                );

                $this->app->flash(
                    'error',
                    $this->app->messages->get(
                        Message::USER_ACCOUNT_ACTIVATION_LINK_EXPIRED,
                        $this->app->language
                    )
                );
                Util::redirector($this->app->urlFor('register'), $urlData);
                break;

            case self::NO_ENTITY_FOUND:
                $this->app->flash(
                    'error',
                    $this->app->messages->get(
                        Message::USER_ACCOUNT_ACTIVATION_LINK_INVALID,
                        $this->app->language
                    )
                ); // TODO
                $this->app->redirect($this->app->urlFor('login'));
            
            default:
                $this->app->flash(
                    'error',
                    $this->app->messages->get(
                        Message::USER_POSTED_DATA_PROCESSING_FAILED,
                        $this->app->language
                    ) . " (109)"
                );
                $this->app->error();
                break;
        }
    }

    public function handleForgotPasswordError ($errorCode, $exception) {
        switch ($errorCode) {
            case self::NO_ENTITY_FOUND:
                $this->app->flash(
                    'error',
                    $this->app->messages->get(
                        Message::USER_NOT_FOUND,
                        $this->app->language
                    )
                    //'L\'identifiant soumis ne correspond a aucun compte.'
                );
                $this->app->redirect($this->app->urlFor('forgot'));
                break;

            case self::USER_NOT_ENABLE_ERROR:
                $this->app->flash(
                    'error',
                    $this->app->messages->get(
                        Message::USER_ACCOUNT_NOT_YET_ACTIVATED,
                        $this->app->language
                    )
                );
                $this->app->redirect($this->app->urlFor('forgot'));
                break;
            
            default:
                $this->app->flash(
                    'error',
                    $this->app->messages->get(
                        Message::USER_POSTED_DATA_PROCESSING_FAILED . ' (108) ' .
                        $exception->getMessage(),
                        $this->app->language
                    )
                );
                $this->app->error();
                break;
        }
    }

    public function handleResetPasswordErrorGet ($errorCode, $exception) {
        switch ($errorCode) {
            // Token doesnt exist
            case self::NO_ENTITY_FOUND:
                $this->app->flash(
                    'error',
                    'Lien érroné. Veuillez reprendre la procedure.'
                );
                $this->app->redirect($this->app->urlFor('forgot'));
                break;

            // Token has expired
            case self::TIME_EXPIRED_ERROR:
                $hash = Util::hash($this->app->randomlib->generateString(128));
                $this->app->session->set('hash', $hash);
                $this->app->flash(
                    'error',
                    'Lien expiré! Un nouveau lien vous a été envoyé.'
                );
                $this->app->redirect(
                    $this->app->urlFor('forgot') .
                    "?status=token-expired&username=" .
                    $this->app->request->get('identify') .
                    "&hash=". $hash
                );
                break;

            // User not enabled - account not activated
            case self::USER_NOT_ENABLE_ERROR:
                $this->app->flash(
                    'error',
                    'Veuillez activer votre compte avant de continuer'
                );
                $this->app->redirect($this->app->urlFor('forgot'));
                break;
            
            default:
                $this->app->flash(
                    'error',
                    'une erreur est lors du traitement des données (107).' .
                    $exception->getMessage()
                );
                $this->app->error();
                break;
        }
    }

    public function handleResetPasswordErrorPost ($errorCode, $exception) {
        switch ($errorCode) {
            case self::NO_ENTITY_FOUND:
                $this->app->flash(
                    'error',
                    'Une erreur s\'est produite. Veuillez reprendre la procedure.'
                );
                $this->app->redirect($this->app->urlFor('forgot'));
                break;
            
            default:
                $this->app->flash('error',
                    'une erreur est lors du traitement des données (106).' .
                    $exception->getMessage()
                );
                $this->app->error();
                break;
        }
    }

    public function handleResendVerificationTokenError ($errorCode, $exception) {
        switch ($errorCode) {
            case self::NO_ENTITY_FOUND:
                $this->app->flash(
                    'error',
                    'L\'identifiant soumis ne correspond a aucun compte.'
                );
                $this->app->redirect($this->app->urlFor('register'));
                break;
            
            case self::USER_ALREADY_ENABLED_ERROR:
                $this->app->flash('info', 'Votre compte a déjà été activé.');
                $this->app->redirect($this->app->urlFor('login'));
                break;

            default:
                $this->app->flash(
                    'error',
                    'une erreur est lors du traitement des données (105).' .
                    $exception->getMessage()
                );
                $this->app->error();
                break;
        }
    }

}