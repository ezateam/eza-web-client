<?php
/**
 * app/Eza/Middelware/TokenHandler.php
 *
 * @author Zangue
 */

namespace Eza\Middleware;

use Slim\Middleware as SlimMiddleware;
use Eza\Interfaces\Middleware\EzaMiddlewareInterface;

use Pest;
use Eza\Lib\Util;

// Define scope combinations
define('R__', 'read');
define('W__', 'write');
define('D__', 'delete');
define('RW_', 'read+write');
define('R_D', 'read+delete');
define('_WD', 'write+delete');
define('RWD', 'read+write+delete');


class TokenHandler extends SlimMiddleware implements EzaMiddlewareInterface  {

	/**
	 * Route scopes
	 * @var array
	 */
	private $scopes;

	/**
	 * Resource url for Token
	 * @var string
	 */
	private $endpoint = '/oauth/token';


	public function __construct() {

		$this->scopes = array(
			'/' => R__,
			'/register' => RW_
		);
	}

	/**
	 * Before handler
	 * @return void
	 */
	public function before () {
		// TODO:
		// Get current time
		// Get access token and expiration date
		// if token expired refresh token or if scope changed
		// if token not set request new token
		$this->app->access_token = $this->getAccessToken();
	}

	/**
	 * After handler
	 * @return void
	 */
	public function after () {
		return;
	}

	/**
	 * Middleware call
	 * @return
	 */
	public function call() {
		$this->before();
		$this->next->call();
		$this->after();
	}

	/**
	 * Get an access token from backend server
	 * @return string
	 */
	private function getAccessToken() {

		$response = null;
		$atoken = null;
		$auth = 'Basic ' . $this->app->config->get('api.backend.tokenkey');
		$restFactory = $this->app->restFactory;

		$rest = $restFactory->create()
			->url($this->endpoint)
			->withHeader('Authorization', $auth)
			->contentType('application/x-www-form-urlencoded')
			->accept('application/json')
			->data('grant_type', 'client_credentials')
			->data('scope', RWD)
			->buildHttpQuery()
			->viaPost()
			->execute();

		if ($rest->succeed()) {
			$response = json_decode($rest->getResponseBody(), true);
			$atoken = $response['access_token'];
		} else {
			// TODO: Handle Errors
		}

		return $atoken;
	}


	private function refreshAccessToken() {

		// TODO:
	}

}