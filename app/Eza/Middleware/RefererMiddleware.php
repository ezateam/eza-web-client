<?php
/**
 * app/Eza/Middelware/RefererMiddleware.php
 *
 * @author Zangue
 */

namespace Eza\Middleware;

use Slim\Middleware as SlimMiddleware;
use Eza\Interfaces\Middleware\EzaMiddlewareInterface;

class RefererMiddleware extends SlimMiddleware implements EzaMiddlewareInterface {


    public function before () {
        return;
    }

    public function after () {
        //echo $this->app->request->getRootUri() .
        //$this->app->request->getResourceUri(); die();
        $rootUri = $this->app->request->getRootUri();
        $resourceUri = $this->app->request->getResourceUri();

        if (in_array($resourceUri, ['/', '/login/', '/register/'])) return;
        if (in_array('partner', explode('/', $resourceUri))) return;

        $this->app->session->set('referer', $rootUri . $resourceUri);
    }

    /**
     * Middleware call
     * @return
     */
    public function call() {
        $this->before();
        $this->next->call();
        $this->after();
    }

}