<?php
/**
 * app/Eza/Middelware/BeforeFilter.php
 *
 * @author Zangue
 */

namespace Eza\Middleware;

use Slim\Middleware as SlimMiddleware;
use Eza\Interfaces\Middleware\EzaMiddlewareInterface;

use Eza\Lib\Util;
use Eza\Model\UserModel;

class BeforeFilter extends SlimMiddleware implements EzaMiddlewareInterface {

	/**
	 * Before request hanfler
	 * @return void
	 */
	public function before () {
		$this->app->hook('slim.before', [$this, 'run']);
	}

	/**
	 * After request handler
	 * @return void
	 */
	public function after () {

	}

	/**
	 * Middleware call
	 * @return
	 */
	public function call() {
		$this->before();
		$this->next->call();
		$this->after();
	}

	public function run() {
		$key = $this->app->config->get('auth.session');

		if ($this->app->session->has($key)) {
			$this->app->auth = $this->app->session->get($key);
			//var_dump($this->app->auth); die();
		}


		//var_dump($this->app->auth); die('lol');

		$this->persistLogin();

		// Append auth infos to view
		$this->app->view->appendData([
			'auth' => $this->app->auth
		]);
	}

	/**
	 * TODO - Rewrite this
	 * Persist the user session if 'remember me' has been checked on login / if 
	 * 'remember me' cookie is present
	 *
	 * @note Need review of this code (logic)
	 * @return [type] [description]
	 */
	protected function persistLogin() {
		$rememberCookie = $this->app->getCookie(
			$this->app->config->get('auth.remember')
		);
		$loggedIn = $this->app->auth;

		//var_dump($loggedIn);

		if ($rememberCookie && !$loggedIn) {
			$credentials = explode('___', $rememberCookie);

			if (empty(trim($rememberCookie)) || count($credentials) !== 2) {
				// TODO - Notify?
				$this->app->redirect($this->app->urlFor('index'));
				return;
			}

			$rememberId = $credentials[0];
			$rememberToken = Util::hash($credentials[1]);

			$userModel = new UserModel($this->app);

			// TODO - to be remove. Get user with remember id instead
			$user = $userModel->getUser($this->app->getCookie('username'));

			if (!$user) return;

			$session = $userModel->getUserSessionByRememberId(
				$user['userUUId'],
				$rememberId
			);

			//var_dump($session, $rememberToken, $user, session_id());
			
			if (Util::hashCheck($session['rememberToken'], $rememberToken)) {
				//echo 'OK! ' . session_id() . ' ' . session_save_path();

				// restore user session
				/*$_SESSION[$this->app->config->get('auth.session')] = array(
						'uuid' => $user['userUUId'],
						'username' => $user['name'],
						'email' => $user['email'],
						'enabled' => $user['enabled'],
						'role' => $user['role']['name']
				);

				// log user back in
				$this->app->auth = $_SESSION[$this->app->config->get(
					'auth.session'
				)];*/

			} else {
				$userModel->deleteUserSession($session['sessionId']);
			}
		}
	}

}