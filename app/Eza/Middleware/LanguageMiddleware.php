<?php
/**
 * app/Eza/Middelware/LanguageMiddleware.php
 *
 * @author Zangue
 */

namespace Eza\Middleware;

use Slim\Middleware as SlimMiddleware;
use Eza\Interfaces\Middleware\EzaMiddlewareInterface;

class LanguageMiddleware extends SlimMiddleware implements EzaMiddlewareInterface {

    public function before () {
        return;
    }

    public function after () {
        return;
    }

    /**
     * Middleware call
     * @return
     */
    public function call() {
        $this->before();
        $this->next->call();
        $this->after();
    }
}