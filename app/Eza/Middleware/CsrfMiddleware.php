<?php
/**
 * app/Eza/Middelware/CsrfMIddleware.php
 *
 * @author Zangue
 */

namespace Eza\Middleware;

use Slim\Middleware as SlimMiddleware;
use Eza\Interfaces\Middleware\EzaMiddlewareInterface;

use Eza\Lib\Util;

class CsrfMIddleware extends SlimMiddleware implements EzaMiddlewareInterface {

	/**
	 * Key property in the $_SESSION array
	 * @var string
	 */
	protected $key;

	/**
	 * Before request handler
	 * @return void
	 */
	public function before () {
		$this->key = $this->app->config->get('csrf.key');
		$this->app->hook('slim.before', [$this, 'check']);
	}

	/**
	 * After request handler
	 * @return void
	 */
	public function after () {
		return;
	}

	/**
	 * Middleware call
	 * @return
	 */
	public function call() {
		$this->before();
		$this->next->call();
		$this->after();
	}

	/**
	 * Check if we need to perform CSRF attack detection
	 * @return boolean
	 */
	private function needCheck () {

		$requestMethod = $this->app->request->getMethod();
		$candidateMethods = ['POST', 'PUT', 'DELETE'];

		return in_array($requestMethod, $candidateMethods);
	}

	/**
	 * Perform on match test on submitted CRSF tokens
	 * @throws Exception on token missmatch (potential CRSF attack)
	 * @return void
	 */
	public function check() {

		$token;
		$csrfHash;
		$submittedToken;

		// TODO - Rewrite this
		// If GET create token and store it
		// IF POST, PUT check submitted token against
		// If tokens doesnt match -> handle
		//var_dump($_SESSION); die();
		// Create csrf token if none exist yet
		if (!$this->app->session->has($this->key) &&
			$this->app->request->isGet()) {
			$csrfHash = Util::hash(
				$this->app->randomlib->generateString(128)
			);

			$this->app->session->set($this->key, $csrfHash);
		}

		$token = $this->app->session->get($this->key);

		if ($this->needCheck()) {
			$submittedToken = $this->app->request->post($this->key) ?: '';

			if (!Util::hashCheck($token, $submittedToken)) {
				// TODO - Handle error. Send email to webmaster
				throw new \Exception("CSRF token mismatch");
			}

			// TODO - Maybe regenerate token here?
		}

		// Store CSRF informations
		$this->app->csrfInfos = array(
			'csrf_key' => $this->key,
			'csrf_token' => $token
		);
	}

}