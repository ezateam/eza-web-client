<?php
/**
 * Client.php
 * 
 * @desc Eza Frontend Application
 * @author Zangue
 */

namespace Eza;

use Slim\Slim;
use Noodlehaus\Config;
use RandomLib\Factory;

use Core\Router;

use Eza\Component\Acl;
use Eza\Component\Message;
use Eza\Component\EzaSessionHandler;

use Eza\Lib\RESTFactory;
use Eza\Lib\SessionData;

use Eza\Middleware\BeforeFilter;
use Eza\Middleware\CsrfMiddleware;
use Eza\Middleware\TokenHandler;
use Eza\Middleware\RefererMiddleware;
use Eza\Middleware\LanguageMiddleware;

class Client {

    /**
     * Slim Application
     * @var Slim
     */
    protected $app;

    protected $router;

    protected $acl;

    /**
     * Installs app class loader on the SPL autoload stack.
     */
    protected function registerAutoloader () {
        require_once CORE_DIR . DS . 'Autoloader.php';

        $coreAutoloader = new \Core\Autoloader();

        $coreAutoloader->register();
    }

    protected function requireDeps () {
        require ROOT . DS . 'vendor' . DS . 'autoload.php';
    }

    protected function setupSession () {
        $sessionHandler = new EzaSessionHandler();

        session_set_save_handler($sessionHandler, true);
        session_cache_limiter(false);
        session_start();
    }

    protected function configurePHP () {
        ini_set('display_errors', 'On');
    }

    /*
     * NOTE: This method *must* be called before routes are added to the
     *       the application.
     * This method perform internationalization of the app.
     */
    protected function i18n () {
        $uri = ltrim($this->app->request->getResourceUri(), '/');
        $uriParts = explode('/', $uri);
        $defaultLang = $this->app->config->get('i18n.default');
        $availableLang = $this->app->config->get('i18n.available');
        $cookieLangValue = $this->app->getCookie('i18n');
        $requestedLang = $defaultLang;

        // Get language from URL
        if (strlen($uriParts[0]) === 2) {
            if (in_array($uriParts[0], $availableLang))
                $requestedLang = $uriParts[0];
        }

        // Set app i18n
        $this->app->language = $requestedLang;
        $this->app->langURLPrefix = "";

        if ($this->app->language !== $defaultLang)
            $this->app->langURLPrefix = "/" . $this->app->language;

        if ($uriParts[0] === $defaultLang) {
            // Default lang routes are not prefixed -> adjust url and redirect
            unset($uriParts[0]);
            $redirectUrl = $this->app->config->get('app.url') .
                '/' . join('/', $uriParts);
            //var_dump($redirectUrl); die();

            /*
             * FIX : Session lost after redirect
             * 1. Use the PHP header() function instead of Slim's redirect()
             * 2. after redirect end the current script with exit()
             * @see http://stackoverflow.com/questions/17242346/php-session-lost-after-redirect
             */
            header('Location: ' . $redirectUrl);
            exit;
        }

        // Config right template path
        $this->app->config(
            'templates.path',
            VIEW_ROOT . DS . $this->app->language . DS . 'Layouts'
        );

        // var_dump(
        //     $this->app->request->getResourceUri(),
        //     $uriParts,
        //     $defaultLang,
        //     $availableLang,
        //     $cookieLangValue,
        //     $_SERVER['HTTP_ACCEPT_LANGUAGE'],
        //     \Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']),
        //     $this->app->language,
        //     $this->app->langURLPrefix
        // ); die();
    }

    protected function createAndSetupApp () {
        /*
         * -- App creation
         * ---------------------------------------------------------------------
         */
        $app = new Slim([
            'mode' => APP_MODE,
            'cookies.encrypt' => true
        ]);

        /*
         * -- Depencies Injections
         * ---------------------------------------------------------------------
         */
        $app->configureMode($app->config('mode'), function () use ($app) {
            $app->config = Config::load(
                APP_DIR . DS . "Config/{$app->mode}.php"
            );
        });

        // Inject Authentication status
        $app->auth = false;

        // Inject referer
        $app->referer = NULL;

        // Inject random generator library
        $rl_factory = new Factory();
        $app->randomlib = $rl_factory->getMediumStrengthGenerator();

        // Inject REST factory
        $app->restFactory = new RESTFactory(
            $app->config->get('api.backend.base_url')
        );

        // Inject session wrapper class to manipulate session data
        $app->session = new SessionData();

        // Inject app messages
        $app->messages = New Message();

        /*
         * -- Middlewares
         * ---------------------------------------------------------------------
         */
        // Add BeforeFilter middleware
        $app->add(new BeforeFilter());

        // Add CSRF protection Middleware
        $app->add(new CsrfMiddleware());

        // Add Token handler middleware:
        // The token handler is responsible to automatically and dynamically
        // generate an acces token that will be used by the app to communicate
        // with the backend server.
        $app->add(new TokenHandler());

        // Add the refere middleware
        $app->add (new RefererMiddleware());

        $this->app = $app;
    }

    protected function setupComponents () {
        // App Acl
        $this->acl = new Acl($this->app);

        // App router
        $this->router = new Router($this->app);
    }

    protected function addErrorHandlers () {
        //$router = $this->router;

        $this->app->notFound(function () {
            $this->router->error('404');
        });

        $this->app->error(function () {
            $this->router->error('500');
        });
    }

    protected function addRoutes () {

    }

    protected function addDefaultRoutes () {

        $this->router->get(
            '/forgot\/',
            'Users@forgotPassword'
        )->name('forgot');

        $this->router->post(
            '/forgot\/',
            'Users@forgotPassword'
        );

        $this->router->get(
            '/resetpassword\/:token',
            'Users@resetPassword'
        )->name('reset');

        $this->router->post(
            '/resetpassword\/',
            'Users@resetPassword'
        )->name('reset-post');

        // Legals
        $this->router->get(
            '/legal/terms-of-use',
            'Pages@termsOfUse'
        )->name('terms-of-use');

        $this->router->get(
            '/legal/privacy-policy',
            'Pages@privacyPolicy'
        )->name('privacy-policy');

        $this->router->get(
            '/legal/cookies-policy',
            'Pages@cookiesPolicy'
        )->name('cookies-policy');
    }

    public function run () {
        $this->configurePHP();

        $this->registerAutoloader();

        $this->requireDeps();

        $this->setupSession();

        $this->createAndSetupApp();

        $this->i18n();

        $this->setupComponents();

        $this->addErrorHandlers();

        $this->addDefaultRoutes();

        $this->addRoutes();

        $this->app->run();
    }
}