<?php
/**
 * app/Controller/MoviesController.php
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

namespace Eza\Controller;

class MoviesController extends AppController {

	/**
	 * Controller name
	 * @var string
	 */
	public $name = 'Movies';


	public function index() {

		// TODO
		// Get all movies and pass data to the view
		return;
	}

	public function search($keywords = null) {

	}

	public function movie($movie = null) {
		// Preview informations
		// Trailer
		// Play button (redirect to watch/xyz)
	}

	public function watch($movie = null) {

		// TODO
		// Get Movie Informations rom backend server
		// Figure out if movie or serie
		// For Series: check if there is many season
		// --- for each season get all episodes. Pass all this infos to the
		// --- view. JS (AJAX) should handle the rest (or not?).
	}

	public function trailer($movie = null) {

		// TODO - e.g check if request is AJAX
		$app = $this->app;
		$this->set(compact('app'));
	}

}