<?php
/**
 * app/Controller/UsersController.php
 *
 * @author Zangue
 */

namespace Eza\Controller;

use Pest;
use PestJSON;
use Exception;
use Eza\Lib\Util;

class PartnersController extends UsersController {

    /**
     * Controller name
     * @var string
     */
    public $name = 'Partners';

    protected $partnerLayout = 'layout_partner.html';

    protected $partnerBackendLayout = 'layout_partner_backend.html';

    public function index () {
        $this->setLayout($this->partnerLayout);
    }

    public function login () {
        $this->setLayout($this->partnerLayout);

        if ($this->app->request->isPost())
            parent::login(true);
    }

    public function logout () {
        $this->setLayout($this->partnerLayout);
        parent::logout(true);
    }

    public function dashboard () {
        $this->setLayout($this->partnerBackendLayout);

    }

    public function becomePartner () {
        $this->setLayout($this->partnerLayout);
    }

}