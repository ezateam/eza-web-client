<?php
/**
 * app/Controller/ErrorsController.php
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

namespace Eza\Controller;

class ErrorsController extends AppController {

	/**
	 * Controller name
	 * @var string
	 */
	public $name = 'Errors';


	public function error404() {

		$this->render($this->app->language . DS . $this->name, '404');

	}

	public function error500() {

		$this->render($this->app->language . DS . $this->name, '500');

	}


}