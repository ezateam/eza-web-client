<?php

/**
 * app/Controller/BaseController.php
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

namespace Eza\Controller;

use Core\Controller\Controller;
use Eza\Lib\Util;

/**
 *  The application controller class. All Controller classes in the application
 *  must extends this class. Use this class to add method that will be common
 *  for all your controllers.
 */
class AppController extends Controller {

	public $name = 'App';

    /**
     * Backend service class
     * @var Backend
     */
    protected $backend;

    /**
     * Error handler class
     * @var ErrorHandler
     */
    protected $errorHandler;

    /**
     * Add error handler to controller
     * @param ErrorHandler $handler
     */
    public function setErrorHandler ($handler) {
        $this->errorHandler = $handler;
    }

    /**
     * Add backend service class to controller
     * @param Backend $backend
     */
    public function setBackend ($backend) {
        $this->backend = $backend;
    }

    /**
     * Generate a random hash string of given length
     * @param  int $length
     * @return string
     */
    protected function randomHash ($length) {
        $hash = Util::hash(
            $this->app->randomlib->generateString(128)
        );

        return $hash;
    }

    /**
     * Get the referer link
     * @return String or NULL
     */
    public function referer () {
        return $this->app->session->get('referer');
    }

}