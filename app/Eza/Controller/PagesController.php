<?php
/**
 * app/Controller/PagesController.php
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

namespace Eza\Controller;

class PagesController extends AppController {

	/**
	 * Controller name
	 * @var string
	 */
	public $name = 'Pages';


	public function index() {

		// Get catalog and pass it to the view
		//var_dump($this->app->access_token);
		$app = $this->app;
		$this->set(compact('app'));

	}

	/**
	 * Term of use page.
	 */
	public function termsOfUse () {
		$app = $this->app;
		$this->set(compact('app'));
	}

	public function privacyPolicy () {
		$app = $this->app;
		$this->set(compact('app'));
	}

	public function cookiesPolicy () {
		$app = $this->app;
		$this->set(compact('app'));
	}

}