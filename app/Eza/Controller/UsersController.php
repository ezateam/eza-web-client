<?php
/**
 * app/Controller/UsersController.php
 *
 * @author Zangue
 */

namespace Eza\Controller;

use Pest;
use PestJSON;
use Exception;
use Eza\Lib\Util;

class UsersController extends AppController {

	const REGISTERED = 'registered';
	const TOKEN_EXPIRED = 'token-expired';
	const RESEND_TOKEN = 'resend-token';
	const ACCOUNT_NOT_ACTIVATED = 'account-not-activated';

	/**
	 * Controller name
	 * @var string
	 */
	public $name = 'Users';

	private function createUserSession ($username) {
		// Avoid Session fixation attack
		session_regenerate_id(true);

		$user = $this->model->getUser($username);
		//var_dump($user);
		$key = $this->app->config->get('auth.session');

		if (is_null($user))
			return false;

		$userInfos = array(
			'uuid'     => $user['userUUId'],
			'username' => $user['name'],
			'email'    => $user['email'],
			'enabled'  => $user['enabled'],
			'role'     => $user['role']['name']
		);

		//$a = $userInfos['role'] !//= 'PRODUCER';
		//var_dump($userInfos);
		//var_dump($a); die();

		// store user session
		$this->app->session->set($key, $userInfos);
		return true;
	}

	/**
	 * Log in user
	 * @return void
	 */
	public function login ($isPartner = false) {
		$request = $this->app->request;

		if ($request->isPost()) {
			$username = $request->post('username');
			$password = $request->post('password');

			$login = $this->backend->login($username, $password);

			// TODO
			//$remember = $request->post('remember');

			if ($login->succeed()) {
				$this->createUserSession($username);
				if ($isPartner)
					$this->app->redirect($this->app->urlFor(
						'partner-dashboard')
					);
				else if ($this->referer())
					$this->app->redirect($this->referer());
				else
					$this->app->redirect($this->app->urlFor('catalog'));
			} else {
				$this->errorHandler->handleLoginError(
					$login->getEzaErrorCode(),
					$login->getException()
				);
			}
		}
	}

	/**
	 * Log out user
	 *
	 * @return void
	 */
	public function logout ($isPartner = false) {
		//$sessionId = session_id();
		//var_dump($this->model->deleteUserSession($sessionId)); //die();
		$this->app->session->destroy();

		/*
		 * If it's desired to kill the session, also delete the session cookie.
		 * Note: This will destroy the session, and not just the session data!
		 */
		if (ini_get("session.use_cookies")) {
		    $params = session_get_cookie_params();
		    setcookie(session_name(), '', time() - 42000,
		        $params["path"], $params["domain"],
		        $params["secure"], $params["httponly"]
		    );
		}

		// TODO - destroy access token
		session_destroy();

		//$this->model->deleteUserSession($sessionId);
		if ($isPartner) {
			$this->app->redirect($this->app->urlFor('partner-index'));
		} else {
			$this->app->redirect($this->app->urlFor('index'));
		}
	}

	private function redirectRegiteredUser () {
		// A has is computed for internal redirections
		$hash = $this->randomHash(128);

		$this->app->session->set('registered', true);
		$this->app->session->set('hash', $hash);

		$urlData = array(
			'status'   => 'registered',
			'username' => $this->app->request->post('username'),
			'hash'     => $hash
		);
		Util::redirector($this->app->urlFor('register'), $urlData);
	}

	private function isInternalRedirectWithParams ($request) {
		if (empty($request->get()))
			return false;
		if (!$request->get('hash'))
			return false;
		if ($request->get('hash') !== $this->app->session->get('hash')) {
			//echo "Hash missmacth"; die();
			$this->app->session->delete('hash');
			return false;
		}

		$this->app->session->delete('hash');
		return true;
	}

	/**
	 * The user just registered.
	 */
	private function handleRegistered () {
		if ($this->app->session->get(self::REGISTERED)) {
			$username = $this->app->request->get('username');

			$this->set(compact('username'));
			$this->setView('registered');
			$this->app->session->delete(self::REGISTERED);
		} else {
			// TODO - Log this and maybe email admin/dev

			// At the moment just redirect to register
			$this->app->redirect($this->app->urlFor('register'));
		}
	}

	/**
	 * Token were expired at time of account confirmation: send new token
	 */
	private function handlerRegisterTokenExpired () {
		if ($this->app->session->get(self::TOKEN_EXPIRED)) {
			$username = $this->app->request->get('username');

			$this->app->session->delete(self::TOKEN_EXPIRED);
			$this->resendVerificationToken($username);
			$this->set(compact('username'));
			$this->setView('registerTokenExpired');
		} else {
			// TODO - Log this and maybe email admin/dev

			// At the moment just redirect to register
			$this->app->redirect($this->app->urlFor('register'));
		}
	}

	/**
	 * After registration, email didnt came. User explicitly want a new token.
	 */
	private function handleRegisterResendToken () {
		if ($this->app->session->get(self::RESEND_TOKEN)) {
			$username = $this->app->request->get('username');

			$this->app->session->delete(self::RESEND_TOKEN);
			$this->resendVerificationToken($username);
			$this->set(compact('username'));
			$this->setView('registerResendToken');
		} else {
			// TODO - Log this and maybe email admin/dev

			// At the moment just redirect to register
			$this->app->redirect($this->app->urlFor('register'));
		}
	}

	/**
	 * The user tried to login without activiting his account: send new token
	 * for activation.
	 */
	private function handleRegisterAccountNotActivated () {
		if ($this->app->session->get('account-not-activated')) {
			$username = $this->app->request->get('username');

			$this->app->session->delete('account-not-activated');
			$this->resendVerificationToken($username);
			$this->set(compact('username'));
			$this->setView('registerAccountNotActivated');
		} else {
			// TODO - Log this and maybe email admin/dev

			// At the moment just redirect to register
			$this->app->redirect($this->app->urlFor('register'));
		}
	}

	/**
	 * Resgister a new user
	 *
	 * @return void
	 */
	public function register ($role = 'CUSTOMER') {
		$request = $this->app->request;

		if ($request->isGet()) {
			if ($this->isInternalRedirectWithParams($request)) {
				switch ($request->get('status')) {
					case self::REGISTERED:
						$this->handleRegistered();
						break;
					case self::TOKEN_EXPIRED:
						$this->handlerRegisterTokenExpired();
						break;
					case self::RESEND_TOKEN:
						$this->handleRegisterResendToken();
						break;
					case self::ACCOUNT_NOT_ACTIVATED:
						$this->handleRegisterAccountNotActivated();
						break;
					default:
						break;
				}
			}
		}

		// Registration action
		if ($request->isPost()) {
			$email = $request->post('email');
			$username = $request->post('username');
			$password = $request->post('password');

			$register = $this->backend->register(
				$email,
				$username,
				$password,
				$role
			);

			if ($register->succeed()) {
				$this->redirectRegiteredUser();
			} else {
				$this->errorHandler->handlerRegisterError(
					$register->getEzaErrorCode(),
					$register->getException()
				);
			}
		}
	}

	/**
	 * Confirm user registration
	 *
	 * @param  string $token registration token
	 * @return void
	 */
	public function confirmRegistration ($token = '') {
		if (!isset($token)) {
			// Silently redirect to index
			$this->app->redirect($this->app->urlFor('index'));
			return;
		}

		$confirm = $this->backend->confirmRegistration($token);

		if ($confirm->succeed()) {
			$usession = $this->createUserSession(
				$this->app->request->get('identify')
			);
			$this->app->flash('success', 'Votre compte a été activé!');

			if ($usession)
				$this->app->redirect($this->app->urlFor('catalog'));
			else
				$this->app->redirect($this->app->urlFor('login'));

		} else {
			$this->errorHandler->handleConfirmRegistrationError(
				$confirm->getEzaErrorCode(),
				$confirm->getException()
			);
		}
	}

	public function resendActivationEmail () { // send new token
		$request = $this->app->request;

		if ($request->isPost()) {
			$username = $request->post('username');
			$hash = $this->randomHash(128);

			$this->app->session->set('resend-token', true);
			$this->app->session->set('hash', $hash);

			$urlData = array(
				'status' => 'resend-token',
				'username' => $username,
				'hash' => $hash
			);

			Util::redirector($this->app->urlFor('register'), $urlData);
		}
	}


	public function resendVerificationToken ($username) {
		$resend = $this->backend->resendVerificationToken($username);

		if ($resend->failed())
			$this->errorHandler->handleResendVerificationTokenError(
				$resend->getEzaErrorCode(),
				$resend->getException()
			);
	}


	private function doForgotPassword ($username) {
		$forgot = $this->backend->forgotPassword($username);
		$posted = false;

		if ($forgot->succeed()) {
			$posted = true;
			$this->set(compact('posted'));
		} else {
			$this->errorHandler->handleForgotPasswordError(
				$forgot->getEzaErrorCode(),
				$forgot->getException()
			);
		}
	}

	public function forgotPassword () {
		$request = $this->app->request;
		$posted = false; // TODO - refactor this var name

		if ($request->isGet()) {
			/*
			 * Internal regirect on this rote only happen when we need to resend
			 * token due to expiration.
			 * FIXME: Bad style code
			 */
			if ($this->isInternalRedirectWithParams($request)) {
				$this->doForgotPassword($request->get('username'));
			} else {
				$this->set(compact('posted'));
				return;
			}

		}

		if ($request->isPost()) {
			$this->doForgotPassword($request->post('username'));
		}
	}


	public function resetPassword ($token = null) {
		$request = $this->app->request;

		if ($request->isGet()) {
			$username = $request->get('identify');

			$this->set(compact('token', 'username'));

			if (!isset($token)) {
				// Silently redirect to index
				$this->app->redirect($this->app->urlFor('index'));
				return;
			}

			$handle = $this->backend->handleVerificationToken($token);

			if ($handle->failed()) {
				$this->errorHandler->handleResetPasswordErrorGet(
					$handle->getEzaErrorCode(),
					$handle->getException()
				);
			}
		}

		if ($request->isPost()) {
			$tk = $request->post('token');
			$password = $request->post('password');
			$username = $request->post('username');

			$reset = $this->backend->resetPassword($tk, $password);

			if ($reset->succeed()) {
				$user = $this->model->getUser($username);
				$isPartner = $user['role']['name'] == 'PRODUCER';

				$this->app->flash('info',
					'Votre mot de passe a été reinitialisé'
				);
				if ($isPartner)
					$this->app->redirect($this->app->urlFor('partner-login'));
				else
					$this->app->redirect($this->app->urlFor('login'));
			} else {
				$this->errorHandler->handleResetPasswordErrorPost(
					$reset->getEzaErrorCode(),
					$reset->getException()
				);
			}
		}
	}


	public function profile () {

		$key = $this->app->config->get('auth.session');
		$username = $this->app->session->get($key)['username'];
		$user = $this->model->getUser($username);
	}

}