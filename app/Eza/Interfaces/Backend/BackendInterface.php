<?php
/**
 * app/Eza/Interface/Backend/BackendInterface.php
 *
 * @author Zangue
 */

namespace Eza\Interfaces\Backend;


/**
 * This is the minimum interface a Backend module shall implement for the
 * frontend to fully work
 */
Interface BackendInterface {

    /**
     * Log user in
     * @param  string $username
     * @param  string $password
     * @return Eza\Lib\REST
     */
    public function login ($username, $password);

    /**
     * Register new user
     * @param  string $email
     * @param  string $username
     * @param  string $password
     * @param  string $role
     * @return Eza\Lib\REST
     */
    public function register ($email, $username, $password, $role);

    /**
     * Confirm user registration with supplied token
     * @param  string $token
     * @return Eza\Lib\REST
     */
    public function confirmRegistration ($token);

    /**
     * Send a new verification token to user
     * @param  string $username
     * @return Eza\Lib\REST
     */
    public function resendVerificationToken ($username);

    /**
     * Send link for password reset
     * @param  string $username
     * @return Eza\Lib\REST
     */
    public function forgotPassword ($username);

    /**
     * Run checks on verification token
     * @param  string $token
     * @return Eza\Lib\REST
     */
    public function handleVerificationToken ($token);

    /**
     * Reset user password
     * @param string $token
     * @param string $password  New password
     * @return Eza\Lib\REST
     */
    public function resetPassword ($token, $password);

    /**
     * GEt user infos
     * @param  String $identifier User name or email
     * @return Eza\Lib\REST
     */
    public function getUser ($identifier);

    /**
     * Get user session infos
     * @param  String $userUUId
     * @param  String $sessionId
     * @return Eza\Lib\REST
     */
    public function getUserSession ($userUUId, $sessionId);

    /**
     * Get all session infos attached to specific user
     * @param  String $userUUId
     * @return Eza\Lib\REST
     */
    public function getAllUserSessions ($userUUId);

    /**
     * Store user session infos
     * @param  Array $data  Session Infos
     * @return Eza\Lib\REST
     */
    public function storeUserSession ($data);

    /**
     * Delete session infos
     * @param  String $sessionId
     * @return Eza\Lib\REST
     */
    public function deleteUserSession ($sessionId);
}