<?php
/**
 * app/Eza/Interface/Middelware/EzaMiddlewareInterface.php
 *
 * @author Zangue
 */

namespace Eza\Interfaces\Middleware;

/**
 * Middleware are wrapper around the application. One typically use when
 * either a action always need to be performed before any code in the application
 * is run or after the application ran or both cases.
 *
 * Each Middleware layer added to this project muss implement this interface
 */
Interface EzaMiddlewareInterface {

    /**
     * Execute every action that needs to be executed before the application
     * runs
     * @return void
     */
    public function before ();

    /**
     * Slim framework needs this interface in order to run the middleware. This
     * method shall exactly do the following:
     *
     * $this->before()
     * $this->next->call()
     * $this->after()
     *
     * @return void
     */
    public function call ();

    /**
     * Execute every action that needs to be executed after the application
     * ran (before the response is sent to the client)
     * @return void
     */
    public function after ();

}