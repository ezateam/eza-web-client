<?php

/**
 * @author Zangue
 */

namespace Eza;

class PartnerClient extends Client {
    
    protected function addRoutes () {
        $this->router->get(
            '/partner\/',
            'Partners@index',
            $this->acl->guestPartner()
        )->name('partner-index');

        $this->router->get(
            '/partner/become-partner\/',
            'Partners@becomePartner',
            $this->acl->guestPartner()
        )->name('become-partner');

        $this->router->get(
            '/partner/login\/',
            'Partners@login',
            $this->acl->guestPartner()
        )->name('partner-login');

        $this->router->post(
            '/partner/login\/',
            'Partners@login',
            $this->acl->guestPartner()
        );

        $this->router->get(
            '/partner/logout\/',
            'Partners@logout',
            $this->acl->partner()
        )->name('partner-logout');

        $this->router->get(
            '/partner/dashboard\/',
            'Partners@dashboard',
            $this->acl->partner()
        )->name('partner-dashboard');
    }

}