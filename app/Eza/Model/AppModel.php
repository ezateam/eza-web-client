<?php

/**
 * app/Eza/Model/BaseModel.php
 *
 * @author Zangue
 */

namespace Eza\Model;

use Core\Model\Model;

/**
 *  The application Model class. All Model classes in the application
 *  must extends this class. Use this class to add method that will be common
 *  for all your models.
 */
class AppModel extends Model {

    public $name = 'App';

    /**
     * Backend service class
     * @var Backend
     */
    protected $backend;


    /**
     * Add backend service class to controller
     * @param Backend $backend
     */
    public function setBackend ($backend) {
        $this->backend = $backend;
    }
}