<?php

/**
 * app/Eza/Model/PartnerModel.php
 *
 * @author Zangue
 */

namespace Eza\Model;

use Pest;

class PartnerModel extends UserModel {

    public $name = 'Partner';

}