<?php

/**
 * app/Eza/Model/UserModel.php
 *
 * @author Zangue
 */

namespace Eza\Model;

use Pest;

class UserModel extends AppModel {

	public $name = 'User';

	/**
	 * return a user object
	 * @param  String $id user identifier: username or email
	 * @return  string array   user details
	 */
	public function getUser($id = NULL) {

		if (!isset($id))
			return NULL;

		$op = $this->backend->getUser($id);

		if ($op->succeed())
			return json_decode($op->getResponseBody(), true);
		else
			// TODO - write logs
			return NULL;
	}

	public function getUserSession($userUUId, $sessionId) {

		$op = $this->backend->getUserSession($userUUId, $sessionId);

		if ($op->succeed())
			return json_decode($op->getResponseBody(), true);
		else
			// TODO - write logs
			return NULL;
	}

	public function getUserSessionAll($userUUId) {

		$op = $this->backend->getAllUserSessions($userUUId, $sessionId);

		if ($op->succeed())
			return json_decode($op->getResponseBody(), true);
		else
			// TODO - write logs
			return NULL;
	}

	public function getUserSessionByRememberId($userUUId, $rememberId) {

		$retval = NULL;

		$sessions = $this->getUserSessionAll($userUUId);

		if (!$sessions)
			return $retval;

		foreach ($sessions as $session) {
			if ($session['rememberId'] === $rememberId) {
				$retval = $session;
				break;
			}
		}

		return $retval;
	}

	public function storeUserSession(
		$userUUId,
		$sessionId,
		$rememberId = NULL,
		$rememberToken = NULL) {

		$clientHost = $this->app->request->getIp();
		$userAgent = $this->app->request->getUserAgent();

		$data = array(
			'sessionId' => $sessionId,
			'clientHost' => $clientHost,
			'userAgent' => $userAgent,
			'rememberId' => $rememberId,
			'rememberToken' => $rememberToken,
			'userUUId' => $userUUId
		);

		$op = $this->backend->storeUserSession($data);

		if ($op->succeed())
			return $op->getResponseBody();
		else
			// TODO - write logs
			return NULL;
	}

	public function deleteUserSession($sessionId) {

		$op = $this->backend->deleteUserSession($sessionId);

		if ($op->succeed())
			return $op->getResponseBody();
		else
			// TODO - write logs
			return NULL;
	}
}