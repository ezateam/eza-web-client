<?php

/**
 * app/Eza/Component/Acl.php
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

namespace Eza\Component;

// TODO - Refactor this class

class Acl {
	
	protected $app;

	public function __construct($app) {
		$this->app = $app;
	}

	protected function authenticationCheck($required) {
		$app = $this->app;

		return function() use ($required, $app) {
			//var_dump($app->auth); die('LLL');
			// Guest only but authenticated -> redirect auth:
			// if an logged in user try is redirected or try to access a guest link
			// Redirect to the corresponding memeber link, i.e, removing 'guest' in
			// the url
			if (!$required && $app->auth) {
				// Try to do matching redirect if uri has guest in it.
				// Redirect to home (catalog) if not
				$uri = $app->request->getResourceUri();
				$uriArray = explode('/', trim($app->request->getResourceUri()));

				if (in_array('guest', $uriArray)) {
					$uri = ltrim($uri, '/guest');
					$uri = rtrim($uri, '/');
					$app->redirect($app->request->getRootUri() . '/' .$uri);
				} else {
					$app->redirect(
						$app->urlFor(
							$app->config->get('auth.login_redirect')
						)
					);
				}
			}

			// Authenticated only but guest -> redirect to login
			// And set flash (You need to be logged in to continue)
			if ($required && !$app->auth) {
				// echo $this->app->messages->get(
				// 	Message::USER_NEED_LOGIN,
				// 	$this->app->language
				// ); die();
				// TODO Flash a message to inform user that he need authentication
				$app->flash(
					'error',
					$this->app->messages->get(
						Message::USER_NEED_LOGIN,
						$this->app->language
					)
				);
				$app->redirect($app->urlFor('login'));
			}

			//if ((!$app->auth && $required) || ($app->auth && !$required)) {
			//	$app->redirect($app->urlFor($app->config->get('auth.login_redirect')));
			//}
		};
	}

	public function member() {
		return $this->authenticationCheck(true);
	}

	public function guest() {
		return $this->authenticationCheck(false);
	}

	public function partner() {

		return function () {
			if ($this->app->auth) {
				// Admin always allowed
				if ($this->app->auth['role'] != 'ADMIN') {
					return;
				}

				// - logged in and user => redirect to catalog
				// else ( producer) allow.
				if ($this->app->auth['role'] != 'PRODUCER') {
					$this->app->flash('info', 'Acces reservé!');
					$this->app->redirect($this->app->urlFor('catalog'));
				}
			}
			// Not logged in => do nothing (allow)
			else {
				$this->app->flash(
					'error',
					'Acces restreint! Pour continuer veuillez vous connecter'
				);
				$this->app->redirect($this->app->urlFor('partner-login'));
			}
		};
	}

	public function guestPartner() {
		return function () {
			if ($this->app->auth) {
				// Admin always allowed
				if ($this->app->auth['role'] == 'CUSTOMER') {
					$this->app->flash('info', 'Acces reservé!');
					$this->app->redirect($this->app->session->get('referer'));
				} else { // Producer already connected
					$this->app->redirect($this->app->urlFor('partner-dashboard'));
				}
			}
		};
	}

}