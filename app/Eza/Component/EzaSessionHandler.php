<?php

/**
 * app/Eza/Component/SessionHandler.php
 *
 * @note   A custom session handler to make database storage of session values
 *         possible
 * @see    http://php.net/manual/en/class.sessionhandlerinterface.php
 * @author Zangue <armand.zangue@gmail.com>
 */

namespace Eza\Component;

use SessionHandlerInterface;
use Eza\Lib\RESTFactory;

use Pest;
use PestJSON;

class EzaSessionHandler implements SessionHandlerInterface {

	/**
	 * Factory to create REST client library objects
	 * @var RESTFactory
	 */
	private $restFactory;

	/**
	 * Backend base URL.
	 *
	 * @var string
	 */
	private $baseUrl;

	/**
	 * API access key
	 *
	 * @var string
	 */
	private $key;

	/**
	 * Class constructor
	 *
	 * @param Slim $app Slim Framework application instance.
	 */
	public function __construct(/*$app*/) {
		//$this->key = $app->config->get('api.phpsession-db.accesskey');
		if (APP_MODE === "development")
			$this->baseUrl = "http://localhost/phpsession-db/api/v1";
		else
			$this->baseUrl = "http://52.28.205.249:8888/api/v1";
	}

	/**
	 * Re-initialize existing session, or creates a new one.
	 * Called when a session starts or when session_start() is invoked.
	 *
	 * @param  string $save_path The path where to store/retrieve the session. 
	 * @param  string $name      The session name.
	 *
	 * @note   We will ignore the parameter since we store session data into
	 *         database instead of filesystem.
	 * 
	 * @return bool
	 */
	public function open($save_path, $name) {

		$this->restFactory = new RESTFactory($this->baseUrl);

		return true;
	}

	/**
	 * Closes the current session. This function is automatically executed
	 * when closing the session, or explicitly via session_write_close().
	 *
	 * @return bool
	 */
	public function close() {

		unset($this->restFactory);

		return true;
	}

	/**
	 * Reads the session data from the session storage, and returns the results.
	 * Called right after the session starts or when session_start() is called.
	 * Please note that before this method is called
	 * SessionHandlerInterface::open() is invoked.
	 *
	 * @param  string $session_id The session id. 
	 * @return string             Returns an encoded string of the read data.
	 *                            If nothing was read, it must return an empty
	 *                            string.
	 */
	public function read($session_id) {

		$endpoint = '/session/' . trim($session_id);
		$response;

		$rest = $this->restFactory->create()
			->url($endpoint)
			->viaGet()
			->execute();

		if ($rest->succeed()) {
			$response = json_decode($rest->getResponseBody(), true);

			// Paranoid
			if ($response['status'] === 'success')
				return $response['data'];
			else
				return "";
		}

		return false;
	}

	/**
	 * Writes the session data to the session storage.
	 * Called by session_write_close(), when session_register_shutdown() fails,
	 * or during a normal shutdown.
	 * Note: SessionHandlerInterface::close() is called immediately after this
	 * 		 function.
	 *
	 * @param  string $session_id   The session id. 
	 * @param  string $session_data The encoded session data. This data is the 
	 *                              result of the PHP internally encoding the 
	 *                              $_SESSION superglobal to a serialized string
	 *                              and passing it as this parameter.
	 *                              Please note sessions use an alternative
	 *                              serialization method.
	 *
	 * @return bool
	 */
	public function write($session_id, $session_data) {
		$endpoint = '/session/write';

		$rest = $this->restFactory->createJSON()
			->url($endpoint)
			->contentType('application/json')
			->data('id', trim($session_id))
			->data('data', trim($session_data))
			->viaPost()
			->execute();

		if ($rest->succeed()) {
			$response = $rest->getResponseBody();

			// Paranoid
			if ($response['status'] === 'success')
				return true;
			else
				return false;
		}
		return false;

	}

	/**
	 * Destroys a session.
	 * Called by session_regenerate_id() (with $destroy = TRUE),
	 * session_destroy() and when session_decode() fails. 
	 *
	 * @param  string $session_id  The session ID being destroyed. 
	 * @return bool
	 */
	public function destroy($session_id) {
		//echo "session destroy called:" . $session_id;

		$endpoint = '/session/destroy/' . trim($session_id);
		$response;

		$rest = $this->restFactory->create()
			->url($endpoint)
			->viaDelete()
			->execute();

		if ($rest->succeed()) {
			$response = json_decode($rest->getResponseBody(), true);

			// Paranoid
			if ($response['status'] === 'success')
				return true;
			else
				return false;
		}
		return false;
	}

	/**
	 * Cleans up expired sessions. Called by session_start(), based on
	 * session.gc_divisor, session.
	 * gc_probability and session.gc_lifetime settings.
	 *
	 * @param  int $maxlifetime Sessions that have not updated for the
	 * last maxlifetime seconds will be removed. 
	 * @return bool
	 */
	public function gc($maxlifetime) {

		$endpoint = '/session/clean/' . trim($maxlifetime);
		$response;

		$rest = $this->restFactory->create()
			->url($endpoint)
			->viaDelete()
			->execute();

		if ($rest->succeed()) {
			$response = json_decode($rest->getResponseBody(), true);

			// Paranoid
			if ($response['status'] === 'success')
				return true;
			else
				return false;
		}
		return false;
	}
}