<?php

/**
 * app/Eza/Component/Message.php
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

namespace Eza\Component;

class Message {
    /*
     * EZA app messages id
     */
    const USER_ACCOUNT_ACTIVATED_SUCCESS = "accountActivatedSuccess";
    const USER_ACCOUNT_ALREADY_ACTIVATED = "userAccountAlreadyActivated";
    const USER_ACCOUNT_NOT_YET_ACTIVATED = "userAccountNotYetActivated";
    const USER_ACCOUNT_ACTIVATION_LINK_EXPIRED = "userAccountActivationLinkExpired";
    const USER_ACCOUNT_ACTIVATION_LINK_INVALID = "userAccountActivationLinkInvalid";
    const USER_ACCOUNT_PASSWORD_RESET_SUCCESS = "userAccountPasswordResetSuccess";
    const USER_LOGIN_WRONG_CREDENTIALS = "userLoginWrongCredentials";
    const USER_NOT_FOUND = "userLoginNotFound";
    const USER_POSTED_DATA_PROCESSING_FAILED = "userPostedDataProcessingFailed";
    const USER_NEED_LOGIN = "userNeedLogin";
    const USER_REGISTER_ID_TAKEN = "userRegisterIDTaken";

    protected $messages = array();

    public function __construct () {
        $this->init();
    }

    public function init () {
        $this->messages[self::USER_ACCOUNT_ACTIVATED_SUCCESS] = array(
                "fr" => "Votre compte a été activé",
                "en" => "Your account is now active"
            );
        $this->messages[self::USER_ACCOUNT_ALREADY_ACTIVATED] = array(
                "fr" => "Votre compte a déjà été activé",
                "en" => "Your account is already active"
            );
        $this->messages[self::USER_ACCOUNT_NOT_YET_ACTIVATED] = array(
                "fr" => "Votre compte n'a pas encore été activé",
                "en" => "Your account is not yet active"
            );
        $this->messages[self::USER_ACCOUNT_ACTIVATION_LINK_EXPIRED] = array(
                "fr" => "La validité du lien d'activation a expiré",
                "en" => "The activation link validity has expired"
            );
        $this->messages[self::USER_ACCOUNT_ACTIVATION_LINK_INVALID] = array(
                "fr" => "Lien d'activation non valide",
                "en" => "Invalid activation link"
            );
        $this->messages[self::USER_ACCOUNT_PASSWORD_RESET_SUCCESS] = array(
                "fr" => "Votre mot de passe a été reinitialisé",
                "en" => "Your password has been resetted"
            );
        $this->messages[self::USER_LOGIN_WRONG_CREDENTIALS] = array(
                "fr" => "L'identifiant et le mot de passe que vous avez soumis ne correspondent pas",
                "en" => "Wrong user name/email or password"
            );
        $this->messages[self::USER_NOT_FOUND] = array(
                "fr" => "L'identifiant soumis ne correspond à aucun compte",
                "en" => "User doesn't exist"
            );
        $this->messages[self::USER_POSTED_DATA_PROCESSING_FAILED] = array(
                "fr" => "Une erreur s'est produite lors du traitement des données",
                "en" => "An error occured while processing your data"
            );
        $this->messages[self::USER_NEED_LOGIN] = array(
                "fr" => "Accèss restreint, veuillez vous connecter",
                "en" => "Please log in to proceed"
            );
        $this->messages[self::USER_REGISTER_ID_TAKEN] = array(
                "fr" => "Un utilisateur avec le nom et/ou le courriel soumis existe déjà",
                "en" => "A user with this name or email already exist"
            );
    }

    public function get($messageId, $languageId) {
        return $this->messages[$messageId][$languageId];
    }
}