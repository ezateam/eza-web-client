<?php
/**
 * app/Eza/Lib/REST.php
 *
 * @author Zangue
 */

namespace Eza\Lib;

use Pest;
use PestJSON;
use Exception;

/**
 * This class offer facilities to easily deal with the PEST PHP RESTful client
 * library
 */
class REST {

    /**
     * The REST object
     * @var Pest | PestJSON | PestXML
     */
    protected $rest = null;

    /**
     * Resource url
     * @var string
     */
    protected $url = "";

    /**
     * Request headers
     * @var array
     */
    protected $headers = array();

    /**
     * Request data
     * @var array
     */
    protected $data = array();

    /**
     * Request status
     * @var boolean
     */
    protected $ok = false;

    /**
     * The choosen request executor
     * @var array
     */
    private $executor = array();

    /**
     * Server response body
     * @var string
     */
    protected $responseBody = NULL;

    /**
     * Exception in case of resquest failure
     * @var null
     */
    protected $exception = NULL;

    /**
     * Class constructor
     * @param string $type PEST PHP type
     * @param string $base Server/Service base URL
     */
    public function __construct($type, $base) {

        if ($type === 'default') {

            $this->rest = new Pest($base);

        } elseif ($type === 'json') {

            $this->rest = new PestJSON($base);

        } elseif ($type === 'xml') {
            $this->rest = new PestXML($base);
        } else {

            throw new Exception("Unknow REST type", 1);
        }

        $this->executor = [$this, 'get'];

    }

    /**
     * Reset the Prest object
     * @return void
     */
    public function reset () {

        $this->url = "";
        $this->headers = array();
        $this->data = array();
        $this->ok = false;
        $this->executor = [$this, 'get'];
        $this->responseBody = NULL;
        $this->exception = NULL;
    }

    /**
     * Set the resource URL
     * @param string $url
     * @return Eza\Lib\REST
     */
    public function url ($url) {

        $this->url = $url;
        return $this;
    }

    /**
     * Get the service endpoint
     * @return string
     */
    public function getUrl () {

        return $this->endpoint;
    }

    /**
     * add request header
     * @param string $key
     * @param string $value
     * @return Eza\Lib\REST
     */
    public function withHeader ($key, $value) {

        $this->headers[] = $key . ': ' . $value;
        return $this;
    }

    /**
     * Get the request headers
     * @return array
     */
    public function getHeaders () {

        return $this->headers;
    }

    /**
     * Set the request content type
     * @param  string $type
     * @return Eza\Lib\REST
     */
    public function contentType ($type) {

        $this->withHeader('Content-Type', $type);
        return $this;
    }

    /**
     * Set the request Accept header
     * @param  string $type
     * @return Eza\Lib\REST
     */
    public function accept ($type) {
        $this->withHeader('Accept', $type);
        return $this;
    }

    /**
     * Pass all request data in array
     * @param array $array
     * @return Eza\Lib\REST
     */
    public function arrayData ($array) {

        if (is_array($array))
            $this->data = $array;
        return $this;
    }

    /**
     * add request data
     * @param string $key
     * @param mixed $value
     * @return Eza\Lib\REST
     */
    public function data ($key, $value) {

        $this->data[$key] = $value;
        return $this;
    }

    /**
     * Get the request data
     * @return array
     */
    public function getData () {

        return $this->data;
    }

    /**
     * Utility function to build http query from request data
     * @return string
     */
    public function buildHttpQuery() {

        if (!empty($this->data))
            $this->data = http_build_query($this->data);

        return $this;
    }

    /**
     * Get request status
     * @return boolean
     */
    public function succeed () {
        return $this->ok;
    }

    /**
     * Get failure status
     * @return boolean
     */
    public function failed () {
        return !$this->ok;
    }

    /**
     * Extract EZA error code from server response
     * @return string
     */
    public function getEzaErrorCode () {

        $retval = "";

        if (NULL !== $this->exception) {
            $retval = explode(' ', $this->exception->getMessage())[0];
        }

        return $retval;
    }

    /**
     * Returns the raised exception in case of failure
     * @return Exception
     */
    public function getException ()
    {
        return $this->exception;
    }

    /**
     * Detect a response header.
     * @param string $header
     * @return boolean
     */
    public function responseHasHeader ($header)
    {
        return NULL !== $this->rest->lastHeader($header);
    }


    /**
     * Return the last response header (case insensitive) or NULL if not present.
     * @param  string $header
     * @return string
     */
    public function getResponseHeader ($header)
    {
        $this->rest->lastHeader($header);
    }

    /**
     * Get last response status
     * @return int
     */
    public function getResponseStatus ()
    {
        return $this->rest->lastStatus();
    }

    /**
     * Get the server response body
     * @return string
     */
    public function getResponseBody () {
        return $this->responseBody;
    }

    /**
     * Perfom a HTTP GET request
     * @throws Exception
     * @return Eza\Lib\REST
     */
    protected function get () {

        try {

            $this->responseBody = $this->rest->get(
                $this->url,
                $this->data,
                $this->headers
            );

            $this->ok = true;

        } catch (Exception $e) {

            $this->ok = false;
            $this->exception = $e;

        } finally {

            return $this;
        }
    }

    /**
     * Perfom a HTTP POST request
     * @throws Exception
     * @return Eza\Lib\REST
     */
    protected function post () {

        try {

            $this->responseBody = $this->rest->post(
                $this->url,
                $this->data,
                $this->headers
            );

            $this->ok = true;

        } catch (Exception $e) {

            $this->ok = false;
            $this->exception = $e;

        } finally {

            return $this;
        }
    }

    /**
     * Perfom a HTTP PUT request
     * @throws Exception
     * @return Eza\Lib\REST
     */
    protected function put () {

        try {

            $this->responseBody = $this->rest->put(
                $this->url,
                $this->data,
                $this->headers
            );

            $this->ok = true;

        } catch (Exception $e) {

            $this->ok = false;
            $this->exception = $e;

        } finally {

            return $this;
        }
    }

    /**
     * Perfom a HTTP PATCH request
     * @throws Exception
     * @return Eza\Lib\REST
     */
    protected function patch () {

        try {

            $this->responseBody = $this->rest->patch(
                $this->url,
                $this->data,
                $this->headers
            );

            $this->ok = true;

        } catch (Exception $e) {

            $this->ok = false;
            $this->exception = $e;

        } finally {

            return $this;
        }
    }

    /**
     * Perfom a HTTP HEAD request
     * @throws Exception
     * @return Eza\Lib\REST
     */
    protected function head () {

        try {

            $this->responseBody = $this->rest->head($this->url);

            $this->ok = true;

        } catch (Exception $e) {

            $this->ok = false;
            $this->exception = $e;

        } finally {

            return $this;
        }
    }

    /**
     * Perfom a HTTP DELETE request
     * @throws Exception
     * @return Eza\Lib\REST
     */
    protected function delete () {

        try {

            $this->responseBody = $this->rest->delete(
                $this->url,
                $this->headers
            );

            $this->ok = true;

        } catch (Exception $e) {

            $this->ok = false;
            $this->exception = $e;

        } finally {

            return $this;
        }
    }

    /**
     * Use HTTP POST method
     * @return Eza\Lib\REST
     */
    public function viaPost () {
        $this->executor = [$this, 'post'];
        return $this;
    }

    /**
     * Use HTTP GET method
     * @return Eza\Lib\REST
     */
    public function viaGet () {
        $this->executor = [$this, 'get'];
        return $this;
    }

    /**
     * Use HTTP PUT method
     * @return Eza\Lib\REST
     */
    public function viaPut () {
        $this->executor = [$this, 'put'];
        return $this;
    }

    /**
     * Use HTTP PATCH method
     * @return Eza\Lib\REST
     */
    public function viaPatch () {
        $this->executor = [$this, 'patch'];
        return $this;
    }

    /**
     * Use HTTP HEAD method
     * @return Eza\Lib\REST
     */
    public function viaHead () {
        $this->executor = [$this, 'head'];
        return $this;
    }

    /**
     * Use HTTP DELETE method
     * @return Eza\Lib\REST
     */
    public function viaDelete () {
        $this->executor = [$this, 'delete'];
        return $this;
    }

    /**
     * Setup authentication
     * @param string $user
     * @param string $pass
     * @param string $auth  'basic' or 'digest'
     * @return Eza\Lib\REST
     */
    public function withAuth ($user, $pass, $auth = 'basic') {
        $this->rest->setupAuth($user, $pass, $auth);
        return $this;
    }

    /**
     * Setup proxy
     * @param string $host
     * @param int $port
     * @param string $user Optional.
     * @param string $pass Optional.
     * @return Eza\Lib\REST
     */
    public function withProxy ($host, $port, $user = NULL, $pass = NULL) {
        $this->rest->setupProxy($host, $port, $user, $pass);
        return $this;
    }

    /**
     * Perform the REST request
     * @return Eza\Lib\REST
     */
    public function execute () {
        // TODO - to be removed
        $this->rest->curl_opts[CURLOPT_SSL_VERIFYPEER] = false;
        $this->rest->curl_opts[CURLOPT_SSL_VERIFYHOST] = false;
        return call_user_func($this->executor);
    }

}