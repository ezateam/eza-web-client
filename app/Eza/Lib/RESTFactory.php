<?php

/**
 * app/Eza/Lib/RESTFactory.php
 *
 * @author Zangue
 */

namespace Eza\Lib;

use Eza\Lib\REST;

/**
 * Factory class for custom REST objects
 */
class RESTFactory {

	/**
	 * The server /service base URL
	 * @var string
	 */
	protected $baseUrl;

	/**
	 * Class contructor
	 * @param string $baseUrl The default base URL
	 */
	public function __construct ($baseUrl) {
		$this->baseUrl = $baseUrl;
	}

	/**
	 * Create a custom REST object
	 * @param  string $type    PEST PHP type
	 * @param  string $baseUrl optional base URL
	 * @return Eza\Lib\REST
	 */
	public function createREST ($type, $baseUrl = NULL) {
		return new REST($type, $baseUrl ? $baseUrl : $this->baseUrl);
	}

	public function create ($baseUrl = NULL) {
		return $this->createREST('default', $baseUrl);
	}

	public function createJSON ($baseUrl = NULL) {
		return $this->createREST('json', $baseUrl);
	}

	public function createXML ($baseUrl = NULL) {
		return $this->createREST('xml', $baseUrl);
	}

}