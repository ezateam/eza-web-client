<?php

/**
 * app/Eza/Lib/SessionData.php
 *
 * @author Zangue
 */

namespace Eza\Lib;

/**
 * This is a utility class that offers a more handy way (hopefully) to manipulate
 * session data
 */
class SessionData {

    /**
     * Class constructor
     */
    public function __construct () {

    }

    /**
     * Store a session data
     * @param string $key
     * @param string $value
     */
    public function set ($key, $value) {

        $_SESSION[$key] = $value;
    }

    /**
     * Get a session data
     * @param  string $key
     * @return mixed
     */
    public function get ($key) {

        if (array_key_exists($key, $_SESSION))
            return $_SESSION[$key];

        return null;
    }

    /**
     * Checks wether a session value is set
     * @param  string  $key
     * @return boolean
     */
    public function has ($key) {
        return array_key_exists($key, $_SESSION);
    }

    /**
     * Delete a session data
     * @param  string $key
     */
    public function delete ($key) {

        if (array_key_exists($key, $_SESSION))
            unset($_SESSION[$key]);
    }

    /**
     * Destroy all session data
     */
    public function destroy () {

        $_SESSION = array();
    }
}