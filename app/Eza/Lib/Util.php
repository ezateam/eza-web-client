<?php

/**
 * app/Eza/Lib/Util.php
 *
 * @desc   Utility class
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

namespace Eza\Lib;

class Util {

	public
    static function hash ($input) {
		return hash('sha256', $input);
	}

	public
    static function hashCheck ($known, $user) {
		return hash_equals($known, $user);
	}

	public
    static function validateUrlHost ($refererHost, $validHost) {

	}

    public
    static function redirector ($url, $args = NULL) {

        $data = NULL;

        if (is_array($args) && !empty($args)) {

            $data = '?';
            $data .= http_build_query($args);

        } else {

            $data = '';

        }

        /*
         * FIX : Session lost after redirect
         * 1. Use the PHP header() function instead of Slim's redirect()
         * 2. after redirect end the current script with exit()
         * @see http://stackoverflow.com/questions/17242346/php-session-lost-after-redirect
         */
        header('Location: ' . $url . $data);
        exit();
    }

    public
    static function getMovieInfoUrl ($app, $id) {
        $movieUrlParts = explode('/', $app->urlFor('movie'));
        $movieUrlParts[count($movieUrl) - 1] = $id;

        return join('/', $movieUrlParts);
    }

    public
    static function getMovieUrl ($app, $id) {
        $movieUrlParts = explode('/', $app->urlFor('movie'));
        $movieUrlParts[count($movieUrl) - 1] = $id;

        return join('/', $movieUrlParts);
    }

}