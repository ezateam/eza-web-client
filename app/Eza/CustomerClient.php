<?php

/**
 * @author Zangue
 */

namespace Eza;

class CustomerClient extends Client {

    protected function addRoutes () {
        $this->router->get(
            '/',
            'Pages@index',
            $this->acl->guest()
        )->name('index');

        $this->router->get(
            '/register\/',
            'Users@register',
            $this->acl->guest()
        )->name('register');

        $this->router->post(
            '/register\/',
            'Users@register',
            $this->acl->guest()
        );

        $this->router->get(
            '/register/confirm/:token\/',
            'Users@confirmRegistration',
            $this->acl->guest()
        )->name('token');

        $this->router->post(
            '/register/resend-activation-email',
            'Users@resendActivationEmail',
            $this->acl->guest()
        )->name('resend-activation-email');

        $this->router->get(
            '/login\/',
            'Users@login',
            $this->acl->guest()
        )->name('login');

        $this->router->post(
            '/login\/',
            'Users@login',
            $this->acl->guest()
        );

        $this->router->get(
            '/logout\/',
            'Users@logout',
            $this->acl->member()
        )->name('logout');

        $this->router->get(
            '/user/profile\/',
            'Users@profile',
            $this->acl->member()
        )->name('user');

        $this->router->post(
            '/user/profile\/',
            'Users@profile',
            $this->acl->member()
        );

        $this->router->get(
            '/catalog\/',
            'Movies@index',
            $this->acl->member()
        )->name('catalog');

        $this->router->get(
            '/guest/catalog\/',
            'Movies@index',
            $this->acl->guest()
        )->name('guest_catalog');

        $this->router->get(
            '/movie/:movie\/',
            'Movies@movie',
            $this->acl->member()
        )->name('movie');

        $this->router->get(
            '/guest/movie/:movie\/',
            'Movies@movie',
            $this->acl->guest()
        )->name('guest_movie');

        $this->router->get(
            '/watch/:movie\/',
            'Movies@watch',
            $this->acl->member()
        )->name('watch');

        $this->router->get(
            '/search',
            'Movies@search'
        )->name('search');

        // Ajax
        $this->router->get(
            '/trailer/:movie',
            'Movies@trailer'
        )->name('trailer');
    }
   
}