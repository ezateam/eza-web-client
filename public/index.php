<?php

/**
 * www/index.php
 *
 * @author Zangue <armand.zangue@gmail.com>
 */

// Bootstrap the application
require APP_DIR . DS . 'bootstrap.php';